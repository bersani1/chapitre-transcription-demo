(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 71);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(2);
var core = __webpack_require__(0);
var ctx = __webpack_require__(11);
var hide = __webpack_require__(9);
var has = __webpack_require__(10);
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),
/* 2 */
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__(33)('wks');
var uid = __webpack_require__(22);
var Symbol = __webpack_require__(2).Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(7);
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(12)(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(4);
var IE8_DOM_DEFINE = __webpack_require__(47);
var toPrimitive = __webpack_require__(28);
var dP = Object.defineProperty;

exports.f = __webpack_require__(5) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(49);
var defined = __webpack_require__(29);
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(6);
var createDesc = __webpack_require__(16);
module.exports = __webpack_require__(5) ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),
/* 10 */
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__(21);
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(48);
var enumBugKeys = __webpack_require__(34);

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(29);
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = {};


/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),
/* 17 */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = true;


/***/ }),
/* 19 */
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__(81)(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__(51)(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),
/* 22 */
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__(6).f;
var has = __webpack_require__(10);
var TAG = __webpack_require__(3)('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _iterator = __webpack_require__(90);

var _iterator2 = _interopRequireDefault(_iterator);

var _symbol = __webpack_require__(95);

var _symbol2 = _interopRequireDefault(_symbol);

var _typeof = typeof _symbol2.default === "function" && typeof _iterator2.default === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = typeof _symbol2.default === "function" && _typeof(_iterator2.default) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof(obj);
} : function (obj) {
  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
};

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(92);
var global = __webpack_require__(2);
var hide = __webpack_require__(9);
var Iterators = __webpack_require__(15);
var TO_STRING_TAG = __webpack_require__(3)('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(73), __esModule: true };

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(7);
var document = __webpack_require__(2).document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(7);
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),
/* 29 */
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__(31);
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),
/* 31 */
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__(33)('keys');
var uid = __webpack_require__(22);
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__(0);
var global = __webpack_require__(2);
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__(18) ? 'pure' : 'global',
  copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
});


/***/ }),
/* 34 */
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),
/* 35 */
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(4);
var dPs = __webpack_require__(83);
var enumBugKeys = __webpack_require__(34);
var IE_PROTO = __webpack_require__(32)('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(27)('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(53).appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__(38);
var ITERATOR = __webpack_require__(3)('iterator');
var Iterators = __webpack_require__(15);
module.exports = __webpack_require__(0).getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__(17);
var TAG = __webpack_require__(3)('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

exports.f = __webpack_require__(3);


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(2);
var core = __webpack_require__(0);
var LIBRARY = __webpack_require__(18);
var wksExt = __webpack_require__(39);
var defineProperty = __webpack_require__(6).f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__(19);
var createDesc = __webpack_require__(16);
var toIObject = __webpack_require__(8);
var toPrimitive = __webpack_require__(28);
var has = __webpack_require__(10);
var IE8_DOM_DEFINE = __webpack_require__(47);
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__(5) ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 25.4.1.5 NewPromiseCapability(C)
var aFunction = __webpack_require__(21);

function PromiseCapability(C) {
  var resolve, reject;
  this.promise = new C(function ($$resolve, $$reject) {
    if (resolve !== undefined || reject !== undefined) throw TypeError('Bad Promise constructor');
    resolve = $$resolve;
    reject = $$reject;
  });
  this.resolve = aFunction(resolve);
  this.reject = aFunction(reject);
}

module.exports.f = function (C) {
  return new PromiseCapability(C);
};


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__(1);
var core = __webpack_require__(0);
var fails = __webpack_require__(12);
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(67);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (obj, key, value) {
  if (key in obj) {
    (0, _defineProperty2.default)(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isIterable2 = __webpack_require__(123);

var _isIterable3 = _interopRequireDefault(_isIterable2);

var _getIterator2 = __webpack_require__(126);

var _getIterator3 = _interopRequireDefault(_getIterator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function sliceIterator(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = (0, _getIterator3.default)(arr), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"]) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  return function (arr, i) {
    if (Array.isArray(arr)) {
      return arr;
    } else if ((0, _isIterable3.default)(Object(arr))) {
      return sliceIterator(arr, i);
    } else {
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    }
  };
}();

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var DEFAULT_PLAYER_ID = exports.DEFAULT_PLAYER_ID = 'DEFAULT_PLAYER_ID';

var BROADCAST_TYPE_REPLAY = exports.BROADCAST_TYPE_REPLAY = 'replay';
var BROADCAST_TYPE_TIMESHIFTING = exports.BROADCAST_TYPE_TIMESHIFTING = 'timeshifting';
var BROADCAST_TYPE_LIVE = exports.BROADCAST_TYPE_LIVE = 'live';
var FORMAT_BEHAVIOUR_FALLBACK = exports.FORMAT_BEHAVIOUR_FALLBACK = 'fallback';
var FORMAT_BEHAVIOUR_OVERRIDE = exports.FORMAT_BEHAVIOUR_OVERRIDE = 'override';
var OUTPUT_TYPE_VIDEO = exports.OUTPUT_TYPE_VIDEO = 'OUTPUT_TYPE_VIDEO';
var OUTPUT_TYPE_AUDIO = exports.OUTPUT_TYPE_AUDIO = 'OUTPUT_TYPE_AUDIO';

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__(5) && !__webpack_require__(12)(function () {
  return Object.defineProperty(__webpack_require__(27)('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__(10);
var toIObject = __webpack_require__(8);
var arrayIndexOf = __webpack_require__(76)(false);
var IE_PROTO = __webpack_require__(32)('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(17);
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(80), __esModule: true };

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(18);
var $export = __webpack_require__(1);
var redefine = __webpack_require__(52);
var hide = __webpack_require__(9);
var Iterators = __webpack_require__(15);
var $iterCreate = __webpack_require__(82);
var setToStringTag = __webpack_require__(23);
var getPrototypeOf = __webpack_require__(54);
var ITERATOR = __webpack_require__(3)('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(9);


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__(2).document;
module.exports = document && document.documentElement;


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(10);
var toObject = __webpack_require__(14);
var IE_PROTO = __webpack_require__(32)('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

// call something on iterator step with safe closing on error
var anObject = __webpack_require__(4);
module.exports = function (iterator, fn, value, entries) {
  try {
    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch (e) {
    var ret = iterator['return'];
    if (ret !== undefined) anObject(ret.call(iterator));
    throw e;
  }
};


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

// check on default Array iterator
var Iterators = __webpack_require__(15);
var ITERATOR = __webpack_require__(3)('iterator');
var ArrayProto = Array.prototype;

module.exports = function (it) {
  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
};


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

var ITERATOR = __webpack_require__(3)('iterator');
var SAFE_CLOSING = false;

try {
  var riter = [7][ITERATOR]();
  riter['return'] = function () { SAFE_CLOSING = true; };
  // eslint-disable-next-line no-throw-literal
  Array.from(riter, function () { throw 2; });
} catch (e) { /* empty */ }

module.exports = function (exec, skipClosing) {
  if (!skipClosing && !SAFE_CLOSING) return false;
  var safe = false;
  try {
    var arr = [7];
    var iter = arr[ITERATOR]();
    iter.next = function () { return { done: safe = true }; };
    arr[ITERATOR] = function () { return iter; };
    exec(arr);
  } catch (e) { /* empty */ }
  return safe;
};


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

var DESCRIPTORS = __webpack_require__(5);
var getKeys = __webpack_require__(13);
var toIObject = __webpack_require__(8);
var isEnum = __webpack_require__(19).f;
module.exports = function (isEntries) {
  return function (it) {
    var O = toIObject(it);
    var keys = getKeys(O);
    var length = keys.length;
    var i = 0;
    var result = [];
    var key;
    while (length > i) {
      key = keys[i++];
      if (!DESCRIPTORS || isEnum.call(O, key)) {
        result.push(isEntries ? [key, O[key]] : O[key]);
      }
    }
    return result;
  };
};


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__(48);
var hiddenKeys = __webpack_require__(34).concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),
/* 60 */
/***/ (function(module, exports) {



/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(108), __esModule: true };

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

// 7.3.20 SpeciesConstructor(O, defaultConstructor)
var anObject = __webpack_require__(4);
var aFunction = __webpack_require__(21);
var SPECIES = __webpack_require__(3)('species');
module.exports = function (O, D) {
  var C = anObject(O).constructor;
  var S;
  return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? D : aFunction(S);
};


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

var ctx = __webpack_require__(11);
var invoke = __webpack_require__(112);
var html = __webpack_require__(53);
var cel = __webpack_require__(27);
var global = __webpack_require__(2);
var process = global.process;
var setTask = global.setImmediate;
var clearTask = global.clearImmediate;
var MessageChannel = global.MessageChannel;
var Dispatch = global.Dispatch;
var counter = 0;
var queue = {};
var ONREADYSTATECHANGE = 'onreadystatechange';
var defer, channel, port;
var run = function () {
  var id = +this;
  // eslint-disable-next-line no-prototype-builtins
  if (queue.hasOwnProperty(id)) {
    var fn = queue[id];
    delete queue[id];
    fn();
  }
};
var listener = function (event) {
  run.call(event.data);
};
// Node.js 0.9+ & IE10+ has setImmediate, otherwise:
if (!setTask || !clearTask) {
  setTask = function setImmediate(fn) {
    var args = [];
    var i = 1;
    while (arguments.length > i) args.push(arguments[i++]);
    queue[++counter] = function () {
      // eslint-disable-next-line no-new-func
      invoke(typeof fn == 'function' ? fn : Function(fn), args);
    };
    defer(counter);
    return counter;
  };
  clearTask = function clearImmediate(id) {
    delete queue[id];
  };
  // Node.js 0.8-
  if (__webpack_require__(17)(process) == 'process') {
    defer = function (id) {
      process.nextTick(ctx(run, id, 1));
    };
  // Sphere (JS game engine) Dispatch API
  } else if (Dispatch && Dispatch.now) {
    defer = function (id) {
      Dispatch.now(ctx(run, id, 1));
    };
  // Browsers with MessageChannel, includes WebWorkers
  } else if (MessageChannel) {
    channel = new MessageChannel();
    port = channel.port2;
    channel.port1.onmessage = listener;
    defer = ctx(port.postMessage, port, 1);
  // Browsers with postMessage, skip WebWorkers
  // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
  } else if (global.addEventListener && typeof postMessage == 'function' && !global.importScripts) {
    defer = function (id) {
      global.postMessage(id + '', '*');
    };
    global.addEventListener('message', listener, false);
  // IE8-
  } else if (ONREADYSTATECHANGE in cel('script')) {
    defer = function (id) {
      html.appendChild(cel('script'))[ONREADYSTATECHANGE] = function () {
        html.removeChild(this);
        run.call(id);
      };
    };
  // Rest old browsers
  } else {
    defer = function (id) {
      setTimeout(ctx(run, id, 1), 0);
    };
  }
}
module.exports = {
  set: setTask,
  clear: clearTask
};


/***/ }),
/* 64 */
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return { e: false, v: exec() };
  } catch (e) {
    return { e: true, v: e };
  }
};


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(4);
var isObject = __webpack_require__(7);
var newPromiseCapability = __webpack_require__(42);

module.exports = function (C, x) {
  anObject(C);
  if (isObject(x) && x.constructor === C) return x;
  var promiseCapability = newPromiseCapability.f(C);
  var resolve = promiseCapability.resolve;
  resolve(x);
  return promiseCapability.promise;
};


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(119), __esModule: true };

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(121), __esModule: true };

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _assign = __webpack_require__(26);

var _assign2 = _interopRequireDefault(_assign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _assign2.default || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(129), __esModule: true };

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(131), __esModule: true };

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(72);


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Transistor = undefined;

var _assign = __webpack_require__(26);

var _assign2 = _interopRequireDefault(_assign);

var _objectWithoutProperties2 = __webpack_require__(78);

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _toConsumableArray2 = __webpack_require__(79);

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _toArray2 = __webpack_require__(86);

var _toArray3 = _interopRequireDefault(_toArray2);

var _values = __webpack_require__(87);

var _values2 = _interopRequireDefault(_values);

var _typeof2 = __webpack_require__(24);

var _typeof3 = _interopRequireDefault(_typeof2);

var _regenerator = __webpack_require__(104);

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(107);

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _keys = __webpack_require__(66);

var _keys2 = _interopRequireDefault(_keys);

var _promise = __webpack_require__(61);

var _promise2 = _interopRequireDefault(_promise);

var _defineProperty2 = __webpack_require__(44);

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends5 = __webpack_require__(68);

var _extends6 = _interopRequireDefault(_extends5);

var _slicedToArray2 = __webpack_require__(45);

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _entries = __webpack_require__(69);

var _entries2 = _interopRequireDefault(_entries);

var _getPrototypeOf = __webpack_require__(70);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(133);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(134);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(135);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _get4 = __webpack_require__(136);

var _get5 = _interopRequireDefault(_get4);

var _inherits2 = __webpack_require__(140);

var _inherits3 = _interopRequireDefault(_inherits2);

var _events = __webpack_require__(148);

var _package = __webpack_require__(149);

var _package2 = _interopRequireDefault(_package);

var _eventHandlers = __webpack_require__(150);

var _eventHandlers2 = _interopRequireDefault(_eventHandlers);

var _utils = __webpack_require__(151);

var _sourcePredicates = __webpack_require__(158);

var sourcePredicates = _interopRequireWildcard(_sourcePredicates);

var _constants = __webpack_require__(46);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Transistor = exports.Transistor = function (_EventEmitter) {
  (0, _inherits3.default)(Transistor, _EventEmitter);

  function Transistor() {
    var plugins = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var initOptions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    (0, _classCallCheck3.default)(this, Transistor);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Transistor.__proto__ || (0, _getPrototypeOf2.default)(Transistor)).call(this));

    (0, _entries2.default)(sourcePredicates).forEach(function (_ref) {
      var _ref2 = (0, _slicedToArray3.default)(_ref, 2),
          name = _ref2[0],
          predicate = _ref2[1];

      _this[name] = predicate;
    });
    _this.setMaxListeners(0); // infinite number of listeners
    _this._eventHandlers = (0, _eventHandlers2.default)(_this);
    _this._currentMedia = null; // Media
    _this._currentSource = 0; // Number
    _this._pipeline = null;
    _this._currentAdapter = null; // Adapter
    _this._availableAdapters = {}; // {string: Adapter class}
    _this._availableEffects = {}; // {string: Effect class}
    _this._preloadedAdapters = {}; // {string: Adapter instance}
    _this._initOptions = initOptions; // {string: Option}
    _this._plugins = [];

    _this.onRequestPause = _this._onRequestPause.bind(_this);
    _this.on(Transistor.ACTION_REQUEST_PAUSE, _this.onRequestPause);
    // Map of plugins per plugin name
    _this._pluginClasses = plugins.reduce(function (previous, plugin) {
      return (0, _extends6.default)({}, previous, (0, _defineProperty3.default)({}, plugin.name, plugin));
    }, {});
    _this._status = Transistor.DefaultStatus;
    return _this;
  }

  (0, _createClass3.default)(Transistor, [{
    key: 'getSelectableSources',
    value: function getSelectableSources() {
      var _ref3 = this.getMedia() || [],
          sources = _ref3.sources;

      var currentSource = this.getCurrentSource();
      return sources.filter(function (_ref4) {
        var broadcastType = _ref4.broadcastType;
        return broadcastType === currentSource.broadcastType;
      }).reduce(function (qualities, source) {
        return qualities.concat(Transistor.normalizeSelectableSource(source));
      }, []);
    }
  }, {
    key: 'constraintToPlayableMatrix',
    value: function constraintToPlayableMatrix(media) {
      return Transistor.constraintToPlayableMatrix(media);
    }
  }, {
    key: 'constraintToFilledValuesInMatrix',
    value: function constraintToFilledValuesInMatrix() {
      return Transistor.constraintToFilledValuesInMatrix();
    }
  }, {
    key: 'constraintToAdapter',
    value: function constraintToAdapter(adapterName) {
      return Transistor.constraintToAdapter(adapterName);
    }
  }, {
    key: 'constraintToFirstSource',
    value: function constraintToFirstSource() {
      return Transistor.constraintToFirstSource();
    }
  }, {
    key: 'constraintToFilteredSources',
    value: function constraintToFilteredSources(sourcePredicate) {
      return Transistor.constraintToFilteredSources(sourcePredicate);
    }
  }, {
    key: 'preloadAdapters',
    value: function preloadAdapters() {
      var _this2 = this;

      this.debug('core', 'Action preloadAdapters');
      return this.debugPromise('core', 'preloadAdapters', _promise2.default.all((0, _keys2.default)(this._availableAdapters).map(function (name) {
        return _this2.preloadAdapter(name);
      })));
    }
  }, {
    key: 'unloadAdapters',
    value: function unloadAdapters() {
      var _this3 = this;

      this.debug('core', 'Action unloadAdapters');
      return this.debugPromise('core', 'unloadAdapters', _promise2.default.all((0, _keys2.default)(this._preloadedAdapters).map(function (name) {
        return _this3.unloadAdapter(name);
      })));
    }
  }, {
    key: 'unloadAdapter',
    value: function unloadAdapter(name) {
      var _this4 = this;

      this.debug('core', 'Action unloadAdapter(\'' + name + '\')');
      return this.debugPromise('core', 'unloadAdapter(\'' + name + '\')', this._preloadedAdapters[name].destroy().then(function () {
        _this4._preloadedAdapters[name] = null;
      }));
    }
  }, {
    key: 'preloadAdapter',
    value: function preloadAdapter(name) {
      this.debug('core', 'Action preloadAdapter(\'' + name + '\')');
      if (this._preloadedAdapters[name]) {
        return this._preloadedAdapters[name];
      }
      if (!this._availableAdapters[name]) {
        throw new Error('Attempted to preload an unknown adapter type \'' + name + '\'');
      }
      this._preloadedAdapters[name] = new this._availableAdapters[name](this);
      return this.debugPromise('core', 'preloadAdapter(\'' + name + '\')', this._preloadedAdapters[name].init());
    }
  }, {
    key: 'setPipeline',
    value: function setPipeline(pipeline) {
      this._pipeline = pipeline;
    }
  }, {
    key: 'getAdapter',
    value: function () {
      var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(name) {
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (this._preloadedAdapters[name]) {
                  _context.next = 4;
                  break;
                }

                _context.next = 3;
                return this.preloadAdapter(name);

              case 3:
                this._preloadedAdapters[name] = _context.sent;

              case 4:
                return _context.abrupt('return', this._preloadedAdapters[name]);

              case 5:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getAdapter(_x3) {
        return _ref5.apply(this, arguments);
      }

      return getAdapter;
    }()
  }, {
    key: 'isStopped',
    value: function isStopped() {
      var _getStatus = this.getStatus(),
          state = _getStatus.state;

      return [this.STATE_READY, this.STATE_PAUSED, this.STATE_ENDED, this.STATE_STOPPED, this.STATE_WAITING].indexOf(state) !== -1;
    }
  }, {
    key: 'emit',
    value: function emit() {
      var _get2, _get3;

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      // Catchall event that notifies about every event
      (_get2 = (0, _get5.default)(Transistor.prototype.__proto__ || (0, _getPrototypeOf2.default)(Transistor.prototype), 'emit', this)).call.apply(_get2, [this, Transistor.EVENT_CATCH_ALL].concat(args));
      this.debug('core', 'Emitting ' + args[0], Array.prototype.slice.call(args, 1));
      (_get3 = (0, _get5.default)(Transistor.prototype.__proto__ || (0, _getPrototypeOf2.default)(Transistor.prototype), 'emit', this)).call.apply(_get3, [this].concat(args));
    }
  }, {
    key: 'reset',
    value: function () {
      var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
        var _this5 = this;

        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                this.emit(this.WILL_RESET, this._initOptions);
                _context2.next = 3;
                return this._removeAdapter();

              case 3:
                this.setStatus({ isInit: false });
                return _context2.abrupt('return', _promise2.default.all(this._plugins.map(function (instance) {
                  return instance.remove && instance.remove(_this5);
                })).then(function () {
                  _this5._plugins = [];
                  _this5.emit(_this5.DID_RESET, {});
                  _this5._initOptions = {};
                }));

              case 5:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function reset() {
        return _ref6.apply(this, arguments);
      }

      return reset;
    }()
  }, {
    key: 'getFirstPlayableSource',
    value: function getFirstPlayableSource(playableMatrix) {
      // If we have an adapter at hand and it can play sources
      if (this._currentAdapter && playableMatrix[this._currentAdapter.name] && playableMatrix[this._currentAdapter.name].length) {
        return {
          adapterName: this._currentAdapter.name,
          sourceIndex: playableMatrix[this._currentAdapter.name][0]
        };
      }

      if (!this._currentAdapter) {
        var _ref7 = (0, _entries2.default)(playableMatrix).find(function (_ref9) {
          var _ref10 = (0, _slicedToArray3.default)(_ref9, 2),
              length = _ref10[1].length;

          return length;
        }) || [],
            _ref8 = (0, _slicedToArray3.default)(_ref7, 2),
            adapterName = _ref8[0],
            _ref8$ = _ref8[1];

        _ref8$ = _ref8$ === undefined ? [] : _ref8$;

        var _ref8$2 = (0, _slicedToArray3.default)(_ref8$, 1),
            firstSource = _ref8$2[0];

        if (adapterName && firstSource !== undefined) {
          return {
            adapterName: adapterName,
            sourceIndex: firstSource
          };
        }
      }

      return {};
    }
  }, {
    key: 'getOption',
    value: function getOption(optionName) {
      return this._initOptions && this._initOptions[optionName] || this._initOptions.plugins && this._currentAdapter && this._initOptions.plugins[this._currentAdapter.adapterName] && this._initOptions.plugins[this._currentAdapter.adapterName][optionName] || this.getPlaybackData()[optionName];
    }
  }, {
    key: '_ifDebug',
    value: function _ifDebug(callback, callbackIfNotDebug) {
      if (callback && this.getOption('debug')) {
        return callback();
      }
      if (callbackIfNotDebug) {
        return callbackIfNotDebug();
      }
      return null;
    }
  }, {
    key: '_padLeft',
    value: function _padLeft(text, length) {
      return text.length < length ? this._padLeft(' ' + text, length) : text;
    }
  }, {
    key: '_getAdapterNamePadding',
    value: function _getAdapterNamePadding() {
      return (0, _keys2.default)(this._availableAdapters).reduce(function (soFar, name) {
        return Math.max(soFar, name.length);
      }, 0);
    }
  }, {
    key: '_checkLogDisplay',
    value: function _checkLogDisplay(message) {
      var debugOptions = this.getOption('debug');
      if ((typeof debugOptions === 'undefined' ? 'undefined' : (0, _typeof3.default)(debugOptions)) !== 'object') {
        return true;
      }
      var whiteList = true;
      var blackList = true;
      if (debugOptions.whiteList) {
        whiteList = debugOptions.whiteList.some(function (requiredString) {
          return message.toLowerCase().includes(requiredString.toLowerCase());
        });
      }
      if (debugOptions.blackList) {
        blackList = !debugOptions.blackList.some(function (forbiddenString) {
          return message.toLowerCase().includes(forbiddenString.toLowerCase());
        });
      }
      return whiteList && blackList;
    }
  }, {
    key: 'debug',
    value: function debug(adapterName) {
      var _this6 = this;

      for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        args[_key2 - 1] = arguments[_key2];
      }

      if (!args.length) {
        throw new Error('called debug with too few arguments');
      }
      return this._ifDebug(function () {
        var message = new Date().toLocaleTimeString() + ' ' + _this6._padLeft(adapterName, _this6._getAdapterNamePadding()) + ' -';
        if (_this6._checkLogDisplay(message + Array.prototype.join.apply(args))) {
          var _console;

          (_console = console).log.apply(_console, [message].concat(args)); // eslint-disable-line no-console
        }
      });
    }
  }, {
    key: 'debugPromise',
    value: function debugPromise(adapterName, message, promise) {
      var _this7 = this;

      if (!adapterName || !message || !promise) {
        throw new Error('called debug with too few arguments');
      }
      return promise.then(function () {
        return _this7._ifDebug(function () {
          return _this7.debug(adapterName, message + ' resolved');
        });
      });
    }
  }, {
    key: 'updateStatusFromOption',
    value: function updateStatusFromOption(options) {
      var statusUpdate = ['muted', 'volume'].reduce(function (soFar, optionName) {
        var value = options[optionName];
        if (value !== undefined) {
          soFar[optionName] = value;
        }
        return soFar;
      }, {});
      return this.setStatus(statusUpdate);
    }
  }, {
    key: '_onRequestSeek',
    value: function _onRequestSeek(time) {
      var _this8 = this;

      this.setStatus({ seekRequest: true });
      var media = this.getMedia();
      return this.loadNewMedia(media, {
        forceStart: true,
        initTime: time,
        constraints: [Transistor.constraintToPlayableMatrix(media), Transistor.constraintToFilledValuesInMatrix(), Transistor.constraintToAdapter(this._currentAdapter && this._currentAdapter.name), Transistor.constraintToFilteredSources(this.isSourceSeekable), Transistor.constraintToFirstSource()]
      }).then(function () {
        _this8.setStatus({
          seekRequest: false,
          seekingTo: null,
          switching: {}
        });
      });
    }
  }, {
    key: '_onRequestPause',
    value: function _onRequestPause() {
      var _this9 = this;

      this.setStatus({ pauseRequest: true });
      try {
        var media = this.getMedia();
        this.loadNewMedia(media, {
          constraints: [Transistor.constraintToPlayableMatrix(media), Transistor.constraintToFilledValuesInMatrix(), Transistor.constraintToAdapter(this._currentAdapter && this._currentAdapter.name), Transistor.constraintToFilteredSources(this.isSourcePauseable, media.sources), Transistor.constraintToFirstSource()]
        }).then(function () {
          return _this9.setCurrentTime(new Date().getTime() / 1000);
        }).then(function () {
          return _this9.pause();
        }).then(function () {
          _this9.setStatus({ pauseRequest: false });
        });
      } catch (error) {
        console.warn('RFPlayer: trying to switch to a pauseable source but none found', error);
      }
    }
  }, {
    key: 'getCurrentAdapter',
    value: function getCurrentAdapter() {
      return this._currentAdapter;
    }
  }, {
    key: 'initPlugins',
    value: function () {
      var _ref11 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3() {
        var _this10 = this;

        var pluginList;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                pluginList = (0, _values2.default)(this._pluginClasses);

                pluginList.sort(function (_ref12, _ref13) {
                  var getConstraints1 = _ref12.getConstraints;
                  var getConstraints2 = _ref13.getConstraints;

                  var _ref14 = getConstraints1 ? getConstraints1() : {},
                      _ref14$loadPriority = _ref14.loadPriority,
                      loadPriority1 = _ref14$loadPriority === undefined ? 0 : _ref14$loadPriority;

                  var _ref15 = getConstraints2 ? getConstraints2() : {},
                      _ref15$loadPriority = _ref15.loadPriority,
                      loadPriority2 = _ref15$loadPriority === undefined ? 0 : _ref15$loadPriority;

                  return loadPriority2 - loadPriority1;
                });

                // Add and init plugins, expose their API
                return _context3.abrupt('return', pluginList.reduce(function (sofar, current) {
                  return sofar.then(function () {
                    return _this10.initPlugin(current);
                  });
                }, _promise2.default.resolve()));

              case 3:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function initPlugins() {
        return _ref11.apply(this, arguments);
      }

      return initPlugins;
    }()
  }, {
    key: 'waitOnce',
    value: function waitOnce(eventName, predicate) {
      var _this11 = this;

      return new _promise2.default(function (resolve) {
        _this11.once(eventName, function (adapter) {
          if (!predicate || predicate(adapter)) {
            return resolve();
          }
          return _this11.waitOnce(eventName, predicate).then(function () {
            return resolve();
          });
        });
      });
    }
  }, {
    key: 'getInitOptions',
    value: function getInitOptions() {
      return this._initOptions;
    }

    /* eslint-disable consistent-return */
    /**
     * Init of RFCorePlayer
     * @param {array<object>} options list of options
     */

  }, {
    key: '_getAvailableAdapters',
    value: function _getAvailableAdapters() {
      return this._availableAdapters;
    }
  }, {
    key: 'getCompatibleAdapters',
    value: function getCompatibleAdapters(playableMatrix, sourceIndex) {
      return (0, _entries2.default)(playableMatrix).filter(function (_ref16) {
        var _ref17 = (0, _slicedToArray3.default)(_ref16, 2),
            playableSourceIndexes = _ref17[1];

        return playableSourceIndexes.includes(sourceIndex);
      });
    }
  }, {
    key: 'init',
    value: function () {
      var _ref18 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(initOptions) {
        var _getStatus2, isInit;

        return _regenerator2.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _getStatus2 = this.getStatus(), isInit = _getStatus2.isInit;

                if (!isInit) {
                  _context4.next = 4;
                  break;
                }

                _context4.next = 4;
                return this.reset();

              case 4:
                if (initOptions) {
                  this._initOptions = initOptions;
                }
                this.updateStatusFromOption(this._initOptions);
                this.emit(Transistor.WILL_INIT, this._initOptions);
                this.setPipeline(this._initOptions.pipeline);
                _context4.next = 10;
                return this.initPlugins();

              case 10:
                _context4.next = 12;
                return this.preloadAdapters();

              case 12:
                this.emit(Transistor.DID_INIT, this._initOptions);
                this.setStatus({ isInit: true });
                return _context4.abrupt('return', _promise2.default.resolve());

              case 15:
              case 'end':
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function init(_x4) {
        return _ref18.apply(this, arguments);
      }

      return init;
    }()
  }, {
    key: 'preload',
    value: function preload() {
      this.debug('core', 'Action preload');
    }
  }, {
    key: '_removeAdapter',
    value: function _removeAdapter() {
      var _this12 = this;

      if (!this._currentAdapter) {
        return;
      }
      return this.unloadAdapter(this._currentAdapter.name).then(function () {
        _this12._currentAdapter = null;
      });
    }
  }, {
    key: '_getDefautHandler',
    value: function _getDefautHandler(name, adapter) {
      var _this13 = this;

      return function () {
        _this13.debug((adapter || _this13._currentAdapter).name, 'event ' + name + ' (unhandled)');
      };
    }
  }, {
    key: 'getHandler',
    value: function getHandler(name, adapter) {
      var handler = void 0;
      if (!(name in this._eventHandlers)) {
        handler = this._getDefautHandler(name, adapter);
      } else {
        handler = this._eventHandlers[name];
      }
      handler = handler.bind(null, adapter);
      return handler;
    }
  }, {
    key: 'extractMediaInfo',
    value: function extractMediaInfo(media) {
      return (0, _extends6.default)({}, media, {
        sources: media.sources.map(function (source) {
          return (0, _extends6.default)({}, source, (0, _utils.getComputedSourceMetadata)(source));
        })
      });
    }
  }, {
    key: 'reloadMedia',
    value: function reloadMedia(options) {
      return this.loadNewMedia(this.getMedia(), options);
    }
  }, {
    key: 'loadNewMedia',
    value: function loadNewMedia(media) {
      var _this14 = this;

      var _ref19 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
          forceStart = _ref19.forceStart,
          autostart = _ref19.autostart,
          _ref19$constraints = _ref19.constraints,
          constraints = _ref19$constraints === undefined ? null : _ref19$constraints;

      var selectedConfiguration = void 0;
      if (constraints === null) {
        selectedConfiguration = (0, _utils.chainConstraints)(this, [Transistor.constraintToPlayableMatrix(media), Transistor.constraintToFilledValuesInMatrix(), Transistor.constraintToAdapter(this._currentAdapter && this._currentAdapter.name), Transistor.constraintToFirstSource()]);
      } else {
        selectedConfiguration = (0, _utils.chainConstraints)(this, constraints);
      }
      var _selectedConfiguratio = selectedConfiguration,
          adapterName = _selectedConfiguratio.adapterName,
          sourceIndex = _selectedConfiguratio.sourceIndex;


      if (!adapterName) {
        this.debug('core', 'Could not find an adapter to play the source');
        this.emit(Transistor.ERROR_NEW_MEDIA);
        return _promise2.default.reject(new Error(Transistor.ERROR_NEW_MEDIA));
      }

      var _getStatus3 = this.getStatus(),
          seekRequest = _getStatus3.seekRequest,
          pauseRequest = _getStatus3.pauseRequest;

      if (seekRequest || pauseRequest) {
        this.setStatus({
          switching: {
            from: this.getCurrentSource(),
            to: this.getMedia().sources[sourceIndex]
          }
        });
      }
      this.setStatus({ nextMedia: {} }); // reset next Media
      this.emit(Transistor.ACTION_BEFORE_NEW_MEDIA);

      // Not to differ event handling too much, we jump out of call stack
      var result = new _promise2.default(function (resolve) {
        setTimeout(function () {
          return resolve();
        }, 0);
      });

      var _getStatus4 = this.getStatus(),
          state = _getStatus4.state;

      if (state === this.STATE_PLAYING || state === this.STATE_PAUSED) {
        this.debug('core', 'loadNewMedia: Stop the current playing media');
        result = result.then(function () {
          return _this14.stop();
        });
      }

      result = result.then(function () {
        _this14._currentMedia = _this14.extractMediaInfo(media);
        _this14._currentSource = sourceIndex;

        // if current adapter cannot play the source
        // or there is no current adapter
        if (!_this14._currentAdapter || adapterName !== _this14._currentAdapter.name) {
          if (_this14._currentAdapter) {
            _this14.hide();
          }
          return _this14.getAdapter(adapterName).then(function (adapter) {
            _this14._currentAdapter = adapter;
          })
          // if adapter uses a third party api, it must be ready when resolving init() promise
          .then(function () {
            _this14.emit(Transistor.PLAYER_CREATED);
          });
        }
      });

      result = result.then(function () {
        _this14.show();

        var _getCurrentSource = _this14.getCurrentSource(),
            broadcastType = _getCurrentSource.broadcastType;

        var _getStatus5 = _this14.getStatus(),
            seekingTo = _getStatus5.seekingTo;

        if (broadcastType === _this14.BROADCAST_TYPE_TIMESHIFTING && typeof seekingTo === 'number') {
          _this14.setStatus({ timeshiftDate: seekingTo });
        }
        if (state === _this14.STATE_PLAYING) {
          forceStart = true;
          autostart = true;
        }
        return _this14._currentAdapter.load(_this14.getMediaUrl(), {
          forceStart: forceStart,
          autostart: autostart
        });
      }).then(function () {
        if (seekRequest || pauseRequest) {
          _this14.setStatus({
            switching: {},
            seekRequest: false,
            pauseRequest: false
          });
        }
        _this14.emit(Transistor.ACTION_NEW_MEDIA);
      }).then(function () {
        if (forceStart) {
          return _this14.play();
        }
      }).then(function () {
        if (_this14._pipeline) {
          _this14.applyPipeline(_this14._pipeline);
        }
      });

      return this.debugPromise('core', 'loadNewMedia', result);
    }
  }, {
    key: 'applyPipeline',
    value: function applyPipeline(pipelineSpec) {
      var _this15 = this;

      var mediaElement = this._currentAdapter.getMediaElement && this._currentAdapter.getMediaElement();
      if (!mediaElement) {
        return;
      }
      mediaElement.crossOrigin = 'anonymous';
      this.audioContext = new AudioContext();
      this.mediaElementSourceNode = this.audioContext.createMediaElementSource(mediaElement);

      this._pipelineNodes = {};

      var _pipelineSpec$reduce = pipelineSpec.reduce(function (soFar, _ref20) {
        var _availableEffects$typ;

        var _ref21 = (0, _toArray3.default)(_ref20),
            type = _ref21[0],
            params = _ref21.slice(1);

        return (_availableEffects$typ = _this15._availableEffects[type]).apply.apply(_availableEffects$typ, [_this15.audioContext, _this15, soFar].concat((0, _toConsumableArray3.default)(params)));
      }, {
        input: null,
        output: null,
        nodes: []
      }),
          input = _pipelineSpec$reduce.input,
          output = _pipelineSpec$reduce.output,
          nodes = _pipelineSpec$reduce.nodes;

      this._pipelineNodes = nodes;
      this.mediaElementSourceNode.connect(input);
      output.connect(this.audioContext.destination);
      if (this.audioContext.state === 'suspended') {
        this.audioContext.resume();
      }
    }
  }, {
    key: 'getPipelineNode',
    value: function getPipelineNode(stage, nodeName) {
      return this._pipelineNodes[stage][nodeName];
    }
  }, {
    key: 'getLowLevelInterface',
    value: function getLowLevelInterface() {
      return this._currentAdapter.getLowLevelInterface();
    }
  }, {
    key: 'toggleMuteVideo',
    value: function toggleMuteVideo() {
      var selector = this._initOptions.selector;

      var videoContainer = document.querySelector('' + selector);
      if (!videoContainer) {
        return;
      }
      if (videoContainer.style.visibility === 'hidden') {
        videoContainer.style.visibility = 'visible';
        this.setStatus({ mutedVideo: false });
      } else {
        videoContainer.style.visibility = 'hidden';
        this.setStatus({ mutedVideo: true });
      }
    }
  }, {
    key: 'play',
    value: function play(time) {
      if (this._currentAdapter.play) {
        return this._currentAdapter.play(time);
      }
      return _promise2.default.resolve();
    }
  }, {
    key: 'pause',
    value: function pause() {
      if (this._currentAdapter.pause) {
        return this._currentAdapter.pause();
      }
      return _promise2.default.resolve();
    }
  }, {
    key: 'toggleMute',
    value: function toggleMute() {
      return this._currentAdapter.setMute();
    }
  }, {
    key: 'stop',
    value: function stop() {
      if (this._currentAdapter && this._currentAdapter.stop) {
        return this._currentAdapter.stop();
      }
      return _promise2.default.resolve();
    }
  }, {
    key: 'hide',
    value: function hide() {
      if (this._currentAdapter && this._currentAdapter.hide) {
        return this._currentAdapter.hide();
      }
    }
  }, {
    key: 'show',
    value: function show() {
      if (this._currentAdapter && this._currentAdapter.show) {
        return this._currentAdapter.show();
      }
    }
  }, {
    key: 'getMediaUrl',
    value: function getMediaUrl() {
      var currentSource = this.getCurrentSource();

      if (currentSource.broadcastType !== this.BROADCAST_TYPE_TIMESHIFTING) {
        return currentSource.url;
      }

      var _getStatus6 = this.getStatus(),
          _getStatus6$timeshift = _getStatus6.timeshiftDate,
          timeshiftDate = _getStatus6$timeshift === undefined ? Math.floor(+new Date() / 1000) : _getStatus6$timeshift;

      var paramSeparator = '?';
      if (currentSource.url.includes('?')) {
        paramSeparator = '&';
      }
      return '' + currentSource.url + paramSeparator + 'date=' + new Date(timeshiftDate * 1000).toISOString();
    }
  }, {
    key: 'setCurrentTime',
    value: function setCurrentTime(time) {
      var _this16 = this;

      this.debug('core', 'Action setCurrentTime');
      this.setStatus({ seekingTo: time });

      if (this._currentAdapter.setCurrentTime) {
        return this._currentAdapter.setCurrentTime(time);
      }

      var result = void 0;

      this.emit(this.TIME_BEFORE_SEEK);

      var currentMedia = this.getMedia();
      var currentSourceIndex = this.getCurrentSourceIndex();
      var currentSource = currentMedia.sources[currentSourceIndex];
      if (!this.isSourceSeekable(currentSource)) {
        this.emit(this.ACTION_REQUEST_SEEK, time);
        result = this._onRequestSeek(time);
      } else if (currentSource.broadcastType === this.BROADCAST_TYPE_TIMESHIFTING) {
        // When media is 'timeshifting', updates media and restart it
        this.setStatus({
          switching: {
            from: this.getCurrentSource(),
            to: this.getCurrentSource()
          },
          timeshiftDate: time
        });
        result = this._currentAdapter.load(this.getMediaUrl(), {
          forceStart: true
        }).then(function () {
          _this16.setStatus({ switching: {} });
        });
      } else if (currentSource.broadcastType !== this.BROADCAST_TYPE_LIVE) {
        result = this.play(time);
      }
      return this.debugPromise(this._currentAdapter.name, 'setCurrentTime', result);
    }
  }, {
    key: 'getTimelinePosition',
    value: function getTimelinePosition() {
      var _getMedia = this.getMedia(),
          startTime = _getMedia.startTime;

      var _ref22 = this.getCurrentSource() || {},
          broadcastType = _ref22.broadcastType;

      if (!broadcastType) {
        return 0;
      }

      var _getStatus7 = this.getStatus(),
          currentTime = _getStatus7.currentTime,
          _getStatus7$timeshift = _getStatus7.timeshiftDate,
          timeshiftDate = _getStatus7$timeshift === undefined ? 0 : _getStatus7$timeshift;

      var duration = this.getDuration();
      var now = Math.floor(+new Date() / 1000);

      switch (broadcastType) {
        case this.BROADCAST_TYPE_LIVE:
          return (now - startTime) / duration;

        case this.BROADCAST_TYPE_TIMESHIFTING:
          return (timeshiftDate + currentTime - startTime) / duration;

        case this.BROADCAST_TYPE_REPLAY:
          return currentTime / duration;

        default:
          return 0;
      }
    }
  }, {
    key: '_hasTimeRange',
    value: function _hasTimeRange() {
      var _getMedia2 = this.getMedia(),
          startTime = _getMedia2.startTime,
          endTime = _getMedia2.endTime;

      return startTime !== undefined && endTime !== undefined;
    }
  }, {
    key: '_checkEndReached',
    value: function _checkEndReached(currentTime) {
      var endReached = false;
      if (this._hasTimeRange()) {
        var _getMedia3 = this.getMedia(),
            startTime = _getMedia3.startTime,
            endTime = _getMedia3.endTime;

        endReached = startTime + currentTime >= endTime;
      }
      return endReached;
    }
  }, {
    key: 'getFullscreenElement',
    value: function getFullscreenElement() {
      var override = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

      if (this.getInitOptions()) {
        return document.querySelector(this.getInitOptions().fullscreenElement || override || '' + this.getInitOptions().selector);
      }
      return null;
    }
  }, {
    key: 'setVolume',
    value: function setVolume(value) {
      return this._currentAdapter.setVolume(value);
    }
  }, {
    key: 'getVolume',
    value: function getVolume() {
      return this._currentAdapter ? this._currentAdapter.getVolume() : 0;
    }
  }, {
    key: 'setFullscreen',
    value: function setFullscreen(activate) {
      var _getCurrentSource2 = this.getCurrentSource(),
          outputType = _getCurrentSource2.outputType;

      if (outputType !== Transistor.OUTPUT_TYPE_VIDEO) {
        return;
      }

      if (activate && Transistor.fullscreenEnabled()) {
        this.debug('core', 'native setFullscreen');
        var fullscreenElement = this.getFullscreenElement();
        Transistor.requestFullscreen(fullscreenElement);
      } else {
        this.debug('core', 'native exitFullscreen');
        Transistor.exitFullscreen();
      }
      this.setStatus({ fullscreen: activate });
    }
  }, {
    key: 'getStatus',
    value: function getStatus() {
      var baseStatus = (0, _extends6.default)({}, Transistor.DefaultStatus, this._status);
      if (this._currentAdapter) {
        return (0, _extends6.default)({}, baseStatus, this.getCurrentSource() ? { currentTime: this._currentAdapter.getCurrentTime() } : {});
      }
      return baseStatus;
    }
  }, {
    key: 'setStatus',
    value: function setStatus(newStatus) {
      this._status = (0, _extends6.default)({}, this._status, newStatus);
      return this._status;
    }
  }, {
    key: 'getMedia',
    value: function getMedia() {
      return this._currentMedia || { sources: [] };
    }
  }, {
    key: 'getContainer',
    value: function getContainer() {
      return document.querySelector('' + this._initOptions.selector);
    }
  }, {
    key: 'getPlaybackData',
    value: function getPlaybackData(timestamp) {
      var t = timestamp;
      if (!timestamp && timestamp !== 0) {
        t = this.getCurrentTime();
      }
      return this.getDataAtTime(t);
    }
  }, {
    key: 'getDataAtTime',
    value: function getDataAtTime(timestamp) {
      var _this17 = this;

      var _ref23 = this.getMedia() || {},
          _ref23$timeMap = _ref23.timeMap,
          timeMap = _ref23$timeMap === undefined ? [] : _ref23$timeMap;

      return timeMap.filter(function (range) {
        return (!range.start || timestamp >= range.start) && (!range.end || timestamp < range.end);
      }).sort(function (a, b) {
        return a.start - b.start;
      }).reduce(function (acc, range) {
        var result = (0, _utils.mergeDeep)(acc, range.data ? range.data : {});
        // metadata defaults to true
        if (!('metadata' in range) || range.metadata) {
          result._metadata = (0, _extends6.default)({}, range, {
            // If start or end are not set, use the whole media as a bound
            start: range.start ? range.start : _this17.startTime || 0,
            end: range.end !== undefined ? range.end : (_this17.startTime || 0) + _this17.getDuration()
          });
          delete result._metadata.data;
        }
        return result;
      }, {});
    }
  }, {
    key: 'updateTimeMap',
    value: function updateTimeMap(newTimeMap) {
      if (!Array.isArray(newTimeMap)) {
        newTimeMap = [newTimeMap];
      }
      if (!this._currentMedia) {
        return;
      }
      this._currentMedia = (0, _extends6.default)({}, this._currentMedia, {
        timeMap: [].concat((0, _toConsumableArray3.default)(this._currentMedia.timeMap || []), (0, _toConsumableArray3.default)(newTimeMap))
      });
      return this._currentMedia;
    }
  }, {
    key: 'updateMediaInfos',
    value: function updateMediaInfos(newMedia) {
      var timeMap = newMedia.timeMap,
          rest = (0, _objectWithoutProperties3.default)(newMedia, ['timeMap']);

      if (timeMap) {
        this.updateTimeMap(timeMap);
      }
      this._currentMedia = (0, _extends6.default)({}, this._currentMedia, rest);
      return this._currentMedia;
    }
  }, {
    key: 'clearTimeMap',
    value: function clearTimeMap() {
      this.debug('core', 'Action clearTimeMap');
      this._currentMedia.timeMap = [];
      return this._currentMedia.timeMap;
    }
  }, {
    key: 'getCurrentTime',
    value: function getCurrentTime() {
      return this.getStatus().currentTime;
    }
  }, {
    key: 'isMute',
    value: function isMute() {
      return this.getStatus().muted;
    }
  }, {
    key: 'calcCurrentTime',
    value: function calcCurrentTime(time) {
      var calcTime = void 0;

      if (time === undefined || time === null) {
        return 0;
      }

      if (this.startTime && this.endTime || this.subMediaInfos.length) {
        if (this.type === Transistor.BROADCAST_TYPE_LIVE) {
          calcTime = new Date().getTime() / 1000;
        } else if (this.type === Transistor.BROADCAST_TYPE_TIMESHIFTING) {
          calcTime = this.timeshiftDate + time;
        } else {
          calcTime = this.startTime && this.endTime ? this.startTime + time : time;
        }
      } else {
        calcTime = time;
      }

      return calcTime;
    }
  }, {
    key: 'getCurrentSourceIndex',
    value: function getCurrentSourceIndex() {
      return this._currentSource;
    }
  }, {
    key: 'getCurrentSource',
    value: function getCurrentSource() {
      var _ref24 = this.getMedia() || {},
          sources = _ref24.sources;

      var currentSourceIndex = this.getCurrentSourceIndex();
      if (currentSourceIndex === undefined || currentSourceIndex === null || !sources) {
        return null;
      }
      return sources[currentSourceIndex];
    }
  }, {
    key: 'setCurrentSource',
    value: function setCurrentSource(index) {
      this._currentSource = index;
    }
  }, {
    key: 'getDuration',
    value: function getDuration() {
      var _getStatus8 = this.getStatus(),
          parsedDuration = _getStatus8.parsedDuration;

      return this._currentMedia && this._currentMedia.startTime && this._currentMedia.endTime && this._currentMedia.endTime - this._currentMedia.startTime || parsedDuration < Infinity && parsedDuration || this._currentMedia && this._currentMedia.duration || 0;
    }
  }, {
    key: 'setDuration',
    value: function setDuration(value) {
      this.setStatus({ parsedDuration: value });
    }
  }, {
    key: 'getAvailablePlaybackRates',
    value: function getAvailablePlaybackRates() {
      if (this._currentAdapter) {
        if (this._currentAdapter.getAvailablePlaybackRates) {
          return this._currentAdapter.getAvailablePlaybackRates();
        }
        return [1];
      }
      return [];
    }
  }, {
    key: 'setPlaybackRate',
    value: function setPlaybackRate(speed) {
      if (this._currentAdapter && this._currentAdapter.setPlaybackRate) {
        return this._currentAdapter.setPlaybackRate(speed);
      }
      return _promise2.default.resolve();
    }
  }, {
    key: 'getPlaybackRate',
    value: function getPlaybackRate() {
      if (this._currentAdapter && this._currentAdapter.getPlaybackRate) {
        return this._currentAdapter.getPlaybackRate();
      }
    }
  }, {
    key: 'addAdapter',
    value: function addAdapter(adapter) {
      this._availableAdapters[adapter.name] = adapter;
    }
  }, {
    key: 'addEffect',
    value: function addEffect(effect) {
      this._availableEffects[effect.name] = effect;
    }
  }, {
    key: 'addEventListener',
    value: function addEventListener(eventType, callback) {
      return this.on(eventType, callback);
    }
  }, {
    key: 'removeEventListener',
    value: function removeEventListener(eventType, callback) {
      this.removeListener(eventType, callback);
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      var _this18 = this;

      this.debug('core', 'Action destroy');
      return this._currentAdapter.destroy().then(function () {
        _this18.emit(Transistor.PLAYER_REMOVED);
        return new _promise2.default(function (resolve) {
          setTimeout(function () {
            _this18.emit(Transistor.STATE_DELETED);
            resolve();
          }, 0);
        });
      });
    }
  }, {
    key: 'setNextMedia',
    value: function setNextMedia(media) {
      this.setStatus({ nextMedia: media });
    }
  }, {
    key: 'removePlugin',
    value: function () {
      var _ref25 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(plugin) {
        return _regenerator2.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return plugin.remove(this);

              case 3:
                _context5.next = 8;
                break;

              case 5:
                _context5.prev = 5;
                _context5.t0 = _context5['catch'](0);

                console.warn('plugin ' + plugin.getName() + ' failed on removal with error ' + _context5.t0);

              case 8:
              case 'end':
                return _context5.stop();
            }
          }
        }, _callee5, this, [[0, 5]]);
      }));

      function removePlugin(_x7) {
        return _ref25.apply(this, arguments);
      }

      return removePlugin;
    }()

    /**
     * add plugin
     * @param {function} plugin add a plugin into Transistor and initialize this
     */

  }, {
    key: 'initPlugin',
    value: function () {
      var _ref26 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(plugin) {
        var _plugin$getPluginMeta, minimumCoreVersion, message;

        return _regenerator2.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                Transistor.REQUIRED_PLUGIN_METHODS.forEach(function (methodName) {
                  if (!plugin[methodName]) {
                    console.warn('found a plugin with no ' + methodName + ' method');
                  }
                });

                if (!Transistor.REQUIRED_PLUGIN_METHODS.some(function (methodName) {
                  return !plugin[methodName];
                })) {
                  _context6.next = 4;
                  break;
                }

                console.warn('ignoring plugin ' + plugin.name);
                return _context6.abrupt('return');

              case 4:
                _plugin$getPluginMeta = plugin.getPluginMetadata(), minimumCoreVersion = _plugin$getPluginMeta.minimumCoreVersion;

                if (!(0, _utils.versionIsGreater)(minimumCoreVersion, _package2.default.version)) {
                  _context6.next = 9;
                  break;
                }

                message = 'plugin ' + plugin.name + ' requires core to have a minimum version of ' + minimumCoreVersion + ', current is ' + _package2.default.version;
                // eslint-disable-next-line

                console.log(message);
                throw new Error(message);

              case 9:
                if (!plugin.isReady) {
                  _context6.next = 11;
                  break;
                }

                return _context6.abrupt('return');

              case 11:
                _context6.prev = 11;
                _context6.next = 14;
                return plugin.init(this);

              case 14:
                _context6.next = 19;
                break;

              case 16:
                _context6.prev = 16;
                _context6.t0 = _context6['catch'](11);

                console.warn('plugin ' + plugin.name + ' failed initializing with error ' + _context6.t0, _context6.t0.stack);

              case 19:
              case 'end':
                return _context6.stop();
            }
          }
        }, _callee6, this, [[11, 16]]);
      }));

      function initPlugin(_x8) {
        return _ref26.apply(this, arguments);
      }

      return initPlugin;
    }()
  }, {
    key: 'addPlugin',
    value: function addPlugin(plugin) {
      this._plugins.push(plugin);
    }
  }, {
    key: 'getComputedSourceMetadata',
    value: function getComputedSourceMetadata() {
      return _utils.getComputedSourceMetadata.apply(undefined, arguments);
    }
  }, {
    key: 'getMimeTypeFromExtension',
    value: function getMimeTypeFromExtension() {
      return _utils.getMimeTypeFromExtension.apply(undefined, arguments);
    }
  }, {
    key: 'DefaultStatus',
    get: function get() {
      return Transistor.DefaultStatus;
    }
  }, {
    key: 'TIMEMAP_WILL_UPDATE',
    get: function get() {
      return Transistor.TIMEMAP_WILL_UPDATE;
    }
  }, {
    key: 'TIMEMAP_DID_UPDATE',
    get: function get() {
      return Transistor.TIMEMAP_DID_UPDATE;
    }
  }, {
    key: 'ACTION_BEFORE_NEW_MEDIA',
    get: function get() {
      return Transistor.ACTION_BEFORE_NEW_MEDIA;
    }
  }, {
    key: 'ACTION_CREATE',
    get: function get() {
      return Transistor.ACTION_CREATE;
    }
  }, {
    key: 'ACTION_NEW_MEDIA',
    get: function get() {
      return Transistor.ACTION_NEW_MEDIA;
    }
  }, {
    key: 'ACTION_SELECT_SOURCE',
    get: function get() {
      return Transistor.ACTION_SELECT_SOURCE;
    }
  }, {
    key: 'ACTION_UPDATE_MEDIAINFOS',
    get: function get() {
      return Transistor.ACTION_UPDATE_MEDIAINFOS;
    }
  }, {
    key: 'ACTION_UPDATE_SUBMEDIAINFOS',
    get: function get() {
      return Transistor.ACTION_UPDATE_SUBMEDIAINFOS;
    }
  }, {
    key: 'OUTPUT_TYPE_AUDIO',
    get: function get() {
      return Transistor.OUTPUT_TYPE_AUDIO;
    }
  }, {
    key: 'OUTPUT_TYPE_VIDEO',
    get: function get() {
      return Transistor.OUTPUT_TYPE_VIDEO;
    }
  }, {
    key: 'ACTION_TIME',
    get: function get() {
      return Transistor.ACTION_TIME;
    }
  }, {
    key: 'ACTION_PLAY',
    get: function get() {
      return Transistor.ACTION_PLAY;
    }
  }, {
    key: 'ACTION_PAUSE',
    get: function get() {
      return Transistor.ACTION_PAUSE;
    }
  }, {
    key: 'ACTION_STOP',
    get: function get() {
      return Transistor.ACTION_STOP;
    }
  }, {
    key: 'ACTION_MUTE',
    get: function get() {
      return Transistor.ACTION_MUTE;
    }
  }, {
    key: 'ACTION_FULLSCREEN',
    get: function get() {
      return Transistor.ACTION_FULLSCREEN;
    }
  }, {
    key: 'ACTION_VOLUME',
    get: function get() {
      return Transistor.ACTION_VOLUME;
    }
  }, {
    key: 'ACTION_DELETE',
    get: function get() {
      return Transistor.ACTION_DELETE;
    }
  }, {
    key: 'ACTION_REQUEST_SEEK',
    get: function get() {
      return Transistor.ACTION_REQUEST_SEEK;
    }
  }, {
    key: 'ACTION_REQUEST_PAUSE',
    get: function get() {
      return Transistor.ACTION_REQUEST_PAUSE;
    }
  }, {
    key: 'STATE_UNREADY',
    get: function get() {
      return Transistor.STATE_UNREADY;
    }
  }, {
    key: 'STATE_READY',
    get: function get() {
      return Transistor.STATE_READY;
    }
  }, {
    key: 'STATE_PLAYING',
    get: function get() {
      return Transistor.STATE_PLAYING;
    }
  }, {
    key: 'STATE_PLAY',
    get: function get() {
      return Transistor.STATE_PLAY;
    }
  }, {
    key: 'STATE_STOPPED',
    get: function get() {
      return Transistor.STATE_STOPPED;
    }
  }, {
    key: 'STATE_PAUSED',
    get: function get() {
      return Transistor.STATE_PAUSED;
    }
  }, {
    key: 'STATE_ENDED',
    get: function get() {
      return Transistor.STATE_ENDED;
    }
  }, {
    key: 'STATE_ERROR',
    get: function get() {
      return Transistor.STATE_ERROR;
    }
  }, {
    key: 'STATE_DELETED',
    get: function get() {
      return Transistor.STATE_DELETED;
    }
  }, {
    key: 'STATE_AD_PLAYING',
    get: function get() {
      return Transistor.STATE_AD_PLAYING;
    }
  }, {
    key: 'STATE_AD_STARTED',
    get: function get() {
      return Transistor.STATE_AD_STARTED;
    }
  }, {
    key: 'STATE_AD_PAUSED',
    get: function get() {
      return Transistor.STATE_AD_PAUSED;
    }
  }, {
    key: 'STATE_AD_STOPPED',
    get: function get() {
      return Transistor.STATE_AD_STOPPED;
    }
  }, {
    key: 'STATE_PLUGINS_LOADED',
    get: function get() {
      return Transistor.STATE_PLUGINS_LOADED;
    }
  }, {
    key: 'FULLSCREEN_ACTIVATED',
    get: function get() {
      return Transistor.FULLSCREEN_ACTIVATED;
    }
  }, {
    key: 'FULLSCREEN_DEACTIVATED',
    get: function get() {
      return Transistor.FULLSCREEN_DEACTIVATED;
    }
  }, {
    key: 'MEDIA_IS_UPDATED',
    get: function get() {
      return Transistor.MEDIA_IS_UPDATED;
    }
  }, {
    key: 'TIME_IS_UPDATED',
    get: function get() {
      return Transistor.TIME_IS_UPDATED;
    }
  }, {
    key: 'AD_TIME_IS_UPDATED',
    get: function get() {
      return Transistor.AD_TIME_IS_UPDATED;
    }
  }, {
    key: 'TIME_BEFORE_SEEK',
    get: function get() {
      return Transistor.TIME_BEFORE_SEEK;
    }
  }, {
    key: 'TIME_SEEK',
    get: function get() {
      return Transistor.TIME_SEEK;
    }
  }, {
    key: 'METADATA_IS_LOADED',
    get: function get() {
      return Transistor.METADATA_IS_LOADED;
    }
  }, {
    key: 'IS_MUTED',
    get: function get() {
      return Transistor.IS_MUTED;
    }
  }, {
    key: 'IS_UNMUTED',
    get: function get() {
      return Transistor.IS_UNMUTED;
    }
  }, {
    key: 'VOLUME_UPDATED',
    get: function get() {
      return Transistor.VOLUME_UPDATED;
    }
  }, {
    key: 'END_REACHED',
    get: function get() {
      return Transistor.END_REACHED;
    }
  }, {
    key: 'PLAYER_CREATED',
    get: function get() {
      return Transistor.PLAYER_CREATED;
    }
  }, {
    key: 'PLAYER_REMOVED',
    get: function get() {
      return Transistor.PLAYER_REMOVED;
    }
  }, {
    key: 'UPDATE_PLUGINS_OPTIONS',
    get: function get() {
      return Transistor.UPDATE_PLUGINS_OPTIONS;
    }
  }, {
    key: 'EVENT_CATCH_ALL',
    get: function get() {
      return Transistor.EVENT_CATCH_ALL;
    }
  }, {
    key: 'STALLED',
    get: function get() {
      return Transistor.STALLED;
    }
  }, {
    key: 'WAITING',
    get: function get() {
      return Transistor.WAITING;
    }
  }, {
    key: 'LOADING',
    get: function get() {
      return Transistor.LOADING;
    }
  }, {
    key: 'WILL_INIT',
    get: function get() {
      return Transistor.WILL_INIT;
    }
  }, {
    key: 'DID_INIT',
    get: function get() {
      return Transistor.DID_INIT;
    }
  }, {
    key: 'MARKER_REACHED',
    get: function get() {
      return Transistor.MARKER_REACHED;
    }
  }, {
    key: 'WILL_RESET',
    get: function get() {
      return Transistor.WILL_RESET;
    }
  }, {
    key: 'DID_RESET',
    get: function get() {
      return Transistor.DID_RESET;
    }
  }, {
    key: 'ERROR',
    get: function get() {
      return Transistor.ERROR;
    }
  }, {
    key: 'ERROR_EVENT',
    get: function get() {
      return Transistor.ERROR_EVENT;
    }
  }, {
    key: 'ERROR_LOAD_MEDIA',
    get: function get() {
      return Transistor.ERROR_LOAD_MEDIA;
    }
  }, {
    key: 'ERROR_CREATED',
    get: function get() {
      return Transistor.ERROR_CREATED;
    }
  }, {
    key: 'ERROR_NEW_MEDIA',
    get: function get() {
      return Transistor.ERROR_NEW_MEDIA;
    }
  }, {
    key: 'ERROR_SELECT_SOURCE',
    get: function get() {
      return Transistor.ERROR_SELECT_SOURCE;
    }
  }, {
    key: 'REQUIRED_PLUGIN_METHODS',
    get: function get() {
      return Transistor.REQUIRED_PLUGIN_METHODS;
    }
  }, {
    key: 'BROADCAST_TYPE_REPLAY',
    get: function get() {
      return Transistor.BROADCAST_TYPE_REPLAY;
    }
  }, {
    key: 'BROADCAST_TYPE_TIMESHIFTING',
    get: function get() {
      return Transistor.BROADCAST_TYPE_TIMESHIFTING;
    }
  }, {
    key: 'BROADCAST_TYPE_LIVE',
    get: function get() {
      return Transistor.BROADCAST_TYPE_LIVE;
    }
  }, {
    key: 'version',
    get: function get() {
      return Transistor.version;
    }
  }], [{
    key: 'getDefaultStatus',
    value: function getDefaultStatus() {
      return {
        state: Transistor.STATE_UNREADY,
        currentTime: 0,
        muted: false,
        volume: 0.75,
        switching: {},
        seekRequest: false,
        pauseRequest: false
      };
    }
  }, {
    key: 'isAKnownFormat',
    value: function isAKnownFormat(source, listMediaType, sourceMetaData) {
      var _ref27 = sourceMetaData || Transistor.getComputedSourceMetadata(source),
          extension = _ref27.extension,
          outputType = _ref27.outputType,
          fallbackExtension = _ref27.fallbackExtension,
          fallbackOutputType = _ref27.fallbackOutputType;

      // Unknown output type or none extension


      if (!listMediaType[outputType] && !listMediaType[fallbackOutputType] || !extension) {
        return false;
      }

      var toLowerCase = function toLowerCase(sequence) {
        return sequence && sequence.map(function (element) {
          return element.toLowerCase && element.toLowerCase();
        });
      };

      // Unknown extension
      if (!(extension && listMediaType[outputType] && toLowerCase(listMediaType[outputType]).includes(extension.toLowerCase())) && !(fallbackExtension && listMediaType[fallbackOutputType] && toLowerCase(listMediaType[fallbackOutputType]).includes(fallbackExtension.toLowerCase()))) {
        return false;
      }
      return true;
    }
  }, {
    key: 'isSupportedByThisBrowser',
    value: function isSupportedByThisBrowser(source, _ref28) {
      var _ref28$testOutputType = _ref28.testOutputType,
          testOutputType = _ref28$testOutputType === undefined ? 'audio' : _ref28$testOutputType,
          sourceMetaData = _ref28.sourceMetaData;

      var _ref29 = sourceMetaData || Transistor.getComputedSourceMetadata(source),
          mimeType = _ref29.mimeType,
          fallbackExtension = _ref29.fallbackExtension;

      if (mimeType) {
        var sourceElement = document.createElement(testOutputType);
        if (sourceElement && sourceElement.canPlayType(mimeType) !== '') {
          return true;
        }
      }
      if (fallbackExtension) {
        var fallbackMimeType = Transistor.getMimeTypeFromExtension(fallbackExtension);
        // Ask the browser to confirm mime-type availability
        var _sourceElement = document.createElement(testOutputType);
        var result = _sourceElement && _sourceElement.canPlayType(fallbackMimeType) !== '';
        return result;
      }
      return false;
    }
  }, {
    key: 'canPlay',
    value: function canPlay(source, listMediaType) {
      var testOutputType = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'audio';

      var sourceMetaData = Transistor.getComputedSourceMetadata(source);
      if (!Transistor.isAKnownFormat(source, listMediaType, sourceMetaData)) {
        return false;
      }
      return Transistor.isSupportedByThisBrowser(source, {
        testOutputType: testOutputType,
        sourceMetaData: sourceMetaData
      });
    }
  }, {
    key: 'normalizeSelectableSource',
    value: function normalizeSelectableSource(selectableSource) {
      var isImmersive = selectableSource.isImmersive,
          nbChannel = selectableSource.nbChannel,
          broadcastType = selectableSource.broadcastType;

      var existAndNorNullOrUndefined = function existAndNorNullOrUndefined(attribute, object) {
        return attribute in object && object[attribute] !== null && object[attribute] !== undefined;
      };
      return (0, _extends6.default)({}, existAndNorNullOrUndefined('isImmersive', selectableSource) ? { isImmersive: isImmersive } : {}, existAndNorNullOrUndefined('nbChannel', selectableSource) ? { nbChannel: nbChannel } : {}, existAndNorNullOrUndefined('broadcastType', selectableSource) ? { broadcastType: broadcastType } : {});
    }
  }, {
    key: 'calcPlayableMatrix',
    value: function calcPlayableMatrix(adapters, sources) {
      return (0, _entries2.default)(adapters).reduce(function (matrix, _ref30) {
        var _ref31 = (0, _slicedToArray3.default)(_ref30, 2),
            adapterName = _ref31[0],
            adapter = _ref31[1];

        return (0, _extends6.default)({}, matrix, (0, _defineProperty3.default)({}, adapterName, sources.map(function (source, index) {
          return adapter.canPlay(source, Transistor) ? index : -1;
        }).filter(function (index) {
          return index !== -1;
        })));
      }, {});
    }
  }, {
    key: 'constraintToPlayableMatrix',
    value: function constraintToPlayableMatrix(media) {
      return function (sourceMatrix, player) {
        return Transistor.calcPlayableMatrix(player._getAvailableAdapters(), media.sources);
      };
    }
  }, {
    key: 'constraintToFilledValuesInMatrix',
    value: function constraintToFilledValuesInMatrix() {
      return function (sourceMatrix) {
        return (0, _keys2.default)(sourceMatrix).reduce(function (cleanedMatrix, key) {
          return sourceMatrix[key].length && sourceMatrix[key].length > 0 ? (0, _assign2.default)((0, _defineProperty3.default)({}, key, sourceMatrix[key]), cleanedMatrix) : cleanedMatrix;
        }, {});
      };
    }
  }, {
    key: 'constraintToAdapter',
    value: function constraintToAdapter(adapterName) {
      return function (sourceMatrix) {
        if (adapterName) {
          var currentAdapterSources = sourceMatrix[adapterName];
          if (currentAdapterSources && currentAdapterSources.length) {
            return (0, _defineProperty3.default)({}, adapterName, currentAdapterSources);
          }
        }
        return sourceMatrix;
      };
    }
  }, {
    key: 'constraintToFirstSource',
    value: function constraintToFirstSource() {
      return function (sourceMatrix) {
        var keys = (0, _keys2.default)(sourceMatrix);
        if (keys.length) {
          return {
            adapterName: keys[0],
            sourceIndex: sourceMatrix[keys[0]][0]
          };
        }
        return {};
      };
    }
  }, {
    key: 'constraintToFilteredSources',
    value: function constraintToFilteredSources(sourcePredicate) {
      return function (sourceMatrix, core) {
        return (0, _entries2.default)(sourceMatrix).reduce(function (soFar, _ref33) {
          var _ref34 = (0, _slicedToArray3.default)(_ref33, 2),
              adapterName = _ref34[0],
              sources = _ref34[1];

          var filteredSources = sources.filter(function (sourceIndex) {
            return sourcePredicate(core.getMedia().sources[sourceIndex]);
          });
          if (filteredSources.length) {
            return (0, _extends6.default)({}, soFar, (0, _defineProperty3.default)({}, adapterName, filteredSources));
          }
          return soFar;
        }, {});
      };
    }
  }, {
    key: 'requestFullscreen',
    value: function requestFullscreen(element) {
      if (element.requestFullscreen) {
        element.requestFullscreen();
      } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
      } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
      } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen();
      }
    }
  }, {
    key: 'exitFullscreen',
    value: function exitFullscreen() {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
  }, {
    key: 'fullscreenEnabled',
    value: function fullscreenEnabled() {
      return document.fullscreenEnabled || document.webkitFullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled;
    }
  }, {
    key: 'TIMEMAP_WILL_UPDATE',
    value: function TIMEMAP_WILL_UPDATE() {
      return 'TIMEMAP_WILL_UPDATE';
    }
  }, {
    key: 'TIMEMAP_DID_UPDATE',
    value: function TIMEMAP_DID_UPDATE() {
      return 'TIMEMAP_DID_UPDATE';
    }
  }, {
    key: 'getComputedSourceMetadata',
    value: function getComputedSourceMetadata() {
      return _utils.getComputedSourceMetadata.apply(undefined, arguments);
    }
  }, {
    key: 'getMimeTypeFromExtension',
    value: function getMimeTypeFromExtension() {
      return _utils.getMimeTypeFromExtension.apply(undefined, arguments);
    }
  }, {
    key: 'DefaultStatus',
    get: function get() {
      return {
        state: Transistor.STATE_UNREADY,
        currentTime: 0,
        muted: false,
        mutedVideo: false,
        nextMedia: {},
        volume: 0.75,
        autostart: false,
        fullscreen: false,
        switching: {},
        isInit: false,
        isStopping: false,
        seekingTo: null
      };
    }
  }, {
    key: 'ACTION_BEFORE_NEW_MEDIA',
    get: function get() {
      return 'ACTION_BEFORE_NEW_MEDIA';
    }
  }, {
    key: 'ACTION_CREATE',
    get: function get() {
      return 'ACTION_CREATE';
    }
  }, {
    key: 'ACTION_NEW_MEDIA',
    get: function get() {
      return 'ACTION_NEW_MEDIA';
    }
  }, {
    key: 'ACTION_SELECT_SOURCE',
    get: function get() {
      return 'ACTION_SELECT_SOURCE';
    }
  }, {
    key: 'ACTION_UPDATE_MEDIAINFOS',
    get: function get() {
      return 'ACTION_UPDATE_MEDIAINFOS';
    }
  }, {
    key: 'ACTION_UPDATE_SUBMEDIAINFOS',
    get: function get() {
      return 'ACTION_UPDATE_SUBMEDIAINFOS';
    }
  }, {
    key: 'OUTPUT_TYPE_AUDIO',
    get: function get() {
      return _constants.OUTPUT_TYPE_AUDIO;
    }
  }, {
    key: 'OUTPUT_TYPE_VIDEO',
    get: function get() {
      return _constants.OUTPUT_TYPE_VIDEO;
    }
  }, {
    key: 'ACTION_TIME',
    get: function get() {
      return 'ACTION_TIME';
    }
  }, {
    key: 'ACTION_PLAY',
    get: function get() {
      return 'ACTION_PLAY';
    }
  }, {
    key: 'ACTION_PAUSE',
    get: function get() {
      return 'ACTION_PAUSE';
    }
  }, {
    key: 'ACTION_STOP',
    get: function get() {
      return 'ACTION_STOP';
    }
  }, {
    key: 'ACTION_MUTE',
    get: function get() {
      return 'ACTION_MUTE';
    }
  }, {
    key: 'ACTION_FULLSCREEN',
    get: function get() {
      return 'ACTION_FULLSCREEN';
    }
  }, {
    key: 'ACTION_VOLUME',
    get: function get() {
      return 'ACTION_VOLUME';
    }
  }, {
    key: 'ACTION_DELETE',
    get: function get() {
      return 'ACTION_DELETE';
    }
  }, {
    key: 'ACTION_REQUEST_SEEK',
    get: function get() {
      return 'ACTION_REQUEST_SEEK';
    }
  }, {
    key: 'ACTION_REQUEST_PAUSE',
    get: function get() {
      return 'ACTION_REQUEST_PAUSE';
    }
  }, {
    key: 'STATE_UNREADY',
    get: function get() {
      return 'STATE_UNREADY';
    }
  }, {
    key: 'STATE_READY',
    get: function get() {
      return 'STATE_READY';
    }
  }, {
    key: 'STATE_PLAYING',
    get: function get() {
      return 'STATE_PLAYING';
    }
  }, {
    key: 'STATE_PLAY',
    get: function get() {
      return 'STATE_PLAY';
    }
  }, {
    key: 'STATE_STOPPED',
    get: function get() {
      return 'STATE_STOPPED';
    }
  }, {
    key: 'STATE_PAUSED',
    get: function get() {
      return 'STATE_PAUSED';
    }
  }, {
    key: 'STATE_ENDED',
    get: function get() {
      return 'STATE_ENDED';
    }
  }, {
    key: 'STATE_ERROR',
    get: function get() {
      return 'STATE_ERROR';
    }
  }, {
    key: 'STATE_DELETED',
    get: function get() {
      return 'STATE_DELETED';
    }
  }, {
    key: 'STATE_AD_PLAYING',
    get: function get() {
      return 'STATE_AD_PLAYING';
    }
  }, {
    key: 'STATE_AD_STARTED',
    get: function get() {
      return 'STATE_AD_STARTED';
    }
  }, {
    key: 'STATE_AD_PAUSED',
    get: function get() {
      return 'STATE_AD_PAUSED';
    }
  }, {
    key: 'STATE_AD_STOPPED',
    get: function get() {
      return 'STATE_AD_STOPPED';
    }
  }, {
    key: 'STATE_PLUGINS_LOADED',
    get: function get() {
      return 'STATE_PLUGINS_LOADED';
    }
  }, {
    key: 'FULLSCREEN_ACTIVATED',
    get: function get() {
      return 'FULLSCREEN_ACTIVATED';
    }
  }, {
    key: 'FULLSCREEN_DEACTIVATED',
    get: function get() {
      return 'FULLSCREEN_DEACTIVATED';
    }
  }, {
    key: 'MEDIA_IS_UPDATED',
    get: function get() {
      return 'MEDIA_IS_UPDATED';
    }
  }, {
    key: 'TIME_IS_UPDATED',
    get: function get() {
      return 'TIME_IS_UPDATED';
    }
  }, {
    key: 'AD_TIME_IS_UPDATED',
    get: function get() {
      return 'AD_TIME_IS_UPDATED';
    }
  }, {
    key: 'TIME_BEFORE_SEEK',
    get: function get() {
      return 'TIME_BEFORE_SEEK';
    }
  }, {
    key: 'TIME_SEEK',
    get: function get() {
      return 'TIME_SEEK';
    }
  }, {
    key: 'METADATA_IS_LOADED',
    get: function get() {
      return 'METADATA_IS_LOADED';
    }
  }, {
    key: 'IS_MUTED',
    get: function get() {
      return 'IS_MUTED';
    }
  }, {
    key: 'IS_UNMUTED',
    get: function get() {
      return 'IS_UNMUTED';
    }
  }, {
    key: 'VOLUME_UPDATED',
    get: function get() {
      return 'VOLUME_UPDATED';
    }
  }, {
    key: 'END_REACHED',
    get: function get() {
      return 'END_REACHED';
    }
  }, {
    key: 'PLAYER_CREATED',
    get: function get() {
      return 'PLAYER_CREATED';
    }
  }, {
    key: 'PLAYER_REMOVED',
    get: function get() {
      return 'PLAYER_REMOVED';
    }
  }, {
    key: 'UPDATE_PLUGINS_OPTIONS',
    get: function get() {
      return 'UPDATE_PLUGINS_OPTIONS';
    }
  }, {
    key: 'EVENT_CATCH_ALL',
    get: function get() {
      return 'EVENT_CATCH_ALL';
    }
  }, {
    key: 'STALLED',
    get: function get() {
      return 'STALLED';
    }
  }, {
    key: 'WAITING',
    get: function get() {
      return 'WAITING';
    }
  }, {
    key: 'LOADING',
    get: function get() {
      return 'LOADING';
    }
  }, {
    key: 'WILL_INIT',
    get: function get() {
      return 'WILL_INIT';
    }
  }, {
    key: 'DID_INIT',
    get: function get() {
      return 'DID_INIT';
    }
  }, {
    key: 'MARKER_REACHED',
    get: function get() {
      return 'MARKER_REACHED';
    }
  }, {
    key: 'WILL_RESET',
    get: function get() {
      return 'WILL_RESET';
    }
  }, {
    key: 'DID_RESET',
    get: function get() {
      return 'DID_RESET';
    }
  }, {
    key: 'ERROR',
    get: function get() {
      return 'ERROR';
    }
  }, {
    key: 'ERROR_EVENT',
    get: function get() {
      return 'ERROR_EVENT';
    }
  }, {
    key: 'ERROR_LOAD_MEDIA',
    get: function get() {
      return 'ERROR_LOAD_MEDIA';
    }
  }, {
    key: 'ERROR_CREATED',
    get: function get() {
      return 'ERROR_CREATED';
    }
  }, {
    key: 'ERROR_NEW_MEDIA',
    get: function get() {
      return 'ERROR_NEW_MEDIA';
    }
  }, {
    key: 'ERROR_SELECT_SOURCE',
    get: function get() {
      return 'ERROR_SELECT_SOURCE';
    }
  }, {
    key: 'REQUIRED_PLUGIN_METHODS',
    get: function get() {
      return ['init', 'getPluginMetadata'];
    }
  }, {
    key: 'BROADCAST_TYPE_REPLAY',
    get: function get() {
      return _constants.BROADCAST_TYPE_REPLAY;
    }
  }, {
    key: 'BROADCAST_TYPE_TIMESHIFTING',
    get: function get() {
      return _constants.BROADCAST_TYPE_TIMESHIFTING;
    }
  }, {
    key: 'BROADCAST_TYPE_LIVE',
    get: function get() {
      return _constants.BROADCAST_TYPE_LIVE;
    }
  }, {
    key: 'version',
    get: function get() {
      return _package2.default.version;
    }
  }]);
  return Transistor;
}(_events.EventEmitter);

(0, _entries2.default)(sourcePredicates).forEach(function (_ref35) {
  var _ref36 = (0, _slicedToArray3.default)(_ref35, 2),
      name = _ref36[0],
      predicate = _ref36[1];

  Transistor[name] = predicate;
});

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(74);
module.exports = __webpack_require__(0).Object.assign;


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.1 Object.assign(target, source)
var $export = __webpack_require__(1);

$export($export.S + $export.F, 'Object', { assign: __webpack_require__(75) });


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 19.1.2.1 Object.assign(target, source, ...)
var DESCRIPTORS = __webpack_require__(5);
var getKeys = __webpack_require__(13);
var gOPS = __webpack_require__(35);
var pIE = __webpack_require__(19);
var toObject = __webpack_require__(14);
var IObject = __webpack_require__(49);
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || __webpack_require__(12)(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) {
      key = keys[j++];
      if (!DESCRIPTORS || isEnum.call(S, key)) T[key] = S[key];
    }
  } return T;
} : $assign;


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(8);
var toLength = __webpack_require__(30);
var toAbsoluteIndex = __webpack_require__(77);
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(31);
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
};

/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _from = __webpack_require__(50);

var _from2 = _interopRequireDefault(_from);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  } else {
    return (0, _from2.default)(arr);
  }
};

/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(20);
__webpack_require__(84);
module.exports = __webpack_require__(0).Array.from;


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(31);
var defined = __webpack_require__(29);
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__(36);
var descriptor = __webpack_require__(16);
var setToStringTag = __webpack_require__(23);
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(9)(IteratorPrototype, __webpack_require__(3)('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(6);
var anObject = __webpack_require__(4);
var getKeys = __webpack_require__(13);

module.exports = __webpack_require__(5) ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var ctx = __webpack_require__(11);
var $export = __webpack_require__(1);
var toObject = __webpack_require__(14);
var call = __webpack_require__(55);
var isArrayIter = __webpack_require__(56);
var toLength = __webpack_require__(30);
var createProperty = __webpack_require__(85);
var getIterFn = __webpack_require__(37);

$export($export.S + $export.F * !__webpack_require__(57)(function (iter) { Array.from(iter); }), 'Array', {
  // 22.1.2.1 Array.from(arrayLike, mapfn = undefined, thisArg = undefined)
  from: function from(arrayLike /* , mapfn = undefined, thisArg = undefined */) {
    var O = toObject(arrayLike);
    var C = typeof this == 'function' ? this : Array;
    var aLen = arguments.length;
    var mapfn = aLen > 1 ? arguments[1] : undefined;
    var mapping = mapfn !== undefined;
    var index = 0;
    var iterFn = getIterFn(O);
    var length, result, step, iterator;
    if (mapping) mapfn = ctx(mapfn, aLen > 2 ? arguments[2] : undefined, 2);
    // if object isn't iterable or it's array with default iterator - use simple case
    if (iterFn != undefined && !(C == Array && isArrayIter(iterFn))) {
      for (iterator = iterFn.call(O), result = new C(); !(step = iterator.next()).done; index++) {
        createProperty(result, index, mapping ? call(iterator, mapfn, [step.value, index], true) : step.value);
      }
    } else {
      length = toLength(O.length);
      for (result = new C(length); length > index; index++) {
        createProperty(result, index, mapping ? mapfn(O[index], index) : O[index]);
      }
    }
    result.length = index;
    return result;
  }
});


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $defineProperty = __webpack_require__(6);
var createDesc = __webpack_require__(16);

module.exports = function (object, index, value) {
  if (index in object) $defineProperty.f(object, index, createDesc(0, value));
  else object[index] = value;
};


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _from = __webpack_require__(50);

var _from2 = _interopRequireDefault(_from);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (arr) {
  return Array.isArray(arr) ? arr : (0, _from2.default)(arr);
};

/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(88), __esModule: true };

/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(89);
module.exports = __webpack_require__(0).Object.values;


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

// https://github.com/tc39/proposal-object-values-entries
var $export = __webpack_require__(1);
var $values = __webpack_require__(58)(false);

$export($export.S, 'Object', {
  values: function values(it) {
    return $values(it);
  }
});


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(91), __esModule: true };

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(20);
__webpack_require__(25);
module.exports = __webpack_require__(39).f('iterator');


/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__(93);
var step = __webpack_require__(94);
var Iterators = __webpack_require__(15);
var toIObject = __webpack_require__(8);

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__(51)(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),
/* 93 */
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),
/* 94 */
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(96), __esModule: true };

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(97);
__webpack_require__(60);
__webpack_require__(102);
__webpack_require__(103);
module.exports = __webpack_require__(0).Symbol;


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// ECMAScript 6 symbols shim
var global = __webpack_require__(2);
var has = __webpack_require__(10);
var DESCRIPTORS = __webpack_require__(5);
var $export = __webpack_require__(1);
var redefine = __webpack_require__(52);
var META = __webpack_require__(98).KEY;
var $fails = __webpack_require__(12);
var shared = __webpack_require__(33);
var setToStringTag = __webpack_require__(23);
var uid = __webpack_require__(22);
var wks = __webpack_require__(3);
var wksExt = __webpack_require__(39);
var wksDefine = __webpack_require__(40);
var enumKeys = __webpack_require__(99);
var isArray = __webpack_require__(100);
var anObject = __webpack_require__(4);
var isObject = __webpack_require__(7);
var toObject = __webpack_require__(14);
var toIObject = __webpack_require__(8);
var toPrimitive = __webpack_require__(28);
var createDesc = __webpack_require__(16);
var _create = __webpack_require__(36);
var gOPNExt = __webpack_require__(101);
var $GOPD = __webpack_require__(41);
var $GOPS = __webpack_require__(35);
var $DP = __webpack_require__(6);
var $keys = __webpack_require__(13);
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function' && !!$GOPS.f;
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function () { return dP(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function (tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    } return setSymbolDesc(it, key, D);
  } return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
  return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  } return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  } return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function (value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__(59).f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__(19).f = $propertyIsEnumerable;
  $GOPS.f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__(18)) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols = (
  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function (key) {
    return has(SymbolRegistry, key += '')
      ? SymbolRegistry[key]
      : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
  },
  useSetter: function () { setter = true; },
  useSimple: function () { setter = false; }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// Chrome 38 and 39 `Object.getOwnPropertySymbols` fails on primitives
// https://bugs.chromium.org/p/v8/issues/detail?id=3443
var FAILS_ON_PRIMITIVES = $fails(function () { $GOPS.f(1); });

$export($export.S + $export.F * FAILS_ON_PRIMITIVES, 'Object', {
  getOwnPropertySymbols: function getOwnPropertySymbols(it) {
    return $GOPS.f(toObject(it));
  }
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) args.push(arguments[i++]);
    $replacer = replacer = args[1];
    if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    if (!isArray(replacer)) replacer = function (key, value) {
      if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(9)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__(22)('meta');
var isObject = __webpack_require__(7);
var has = __webpack_require__(10);
var setDesc = __webpack_require__(6).f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__(12)(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

// all enumerable object keys, includes symbols
var getKeys = __webpack_require__(13);
var gOPS = __webpack_require__(35);
var pIE = __webpack_require__(19);
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
  } return result;
};


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__(17);
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__(8);
var gOPN = __webpack_require__(59).f;
var toString = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(40)('asyncIterator');


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(40)('observable');


/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(105);


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// This method of obtaining a reference to the global object needs to be
// kept identical to the way it is obtained in runtime.js
var g = (function() { return this })() || Function("return this")();

// Use `getOwnPropertyNames` because not all browsers support calling
// `hasOwnProperty` on the global `self` object in a worker. See #183.
var hadRuntime = g.regeneratorRuntime &&
  Object.getOwnPropertyNames(g).indexOf("regeneratorRuntime") >= 0;

// Save the old regeneratorRuntime in case it needs to be restored later.
var oldRuntime = hadRuntime && g.regeneratorRuntime;

// Force reevalutation of runtime.js.
g.regeneratorRuntime = undefined;

module.exports = __webpack_require__(106);

if (hadRuntime) {
  // Restore the original runtime.
  g.regeneratorRuntime = oldRuntime;
} else {
  // Remove the global property added by runtime.js.
  try {
    delete g.regeneratorRuntime;
  } catch(e) {
    g.regeneratorRuntime = undefined;
  }
}


/***/ }),
/* 106 */
/***/ (function(module, exports) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

!(function(global) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  var inModule = typeof module === "object";
  var runtime = global.regeneratorRuntime;
  if (runtime) {
    if (inModule) {
      // If regeneratorRuntime is defined globally and we're in a module,
      // make the exports object identical to regeneratorRuntime.
      module.exports = runtime;
    }
    // Don't bother evaluating the rest of this file if the runtime was
    // already defined globally.
    return;
  }

  // Define the runtime globally (as expected by generated code) as either
  // module.exports (if we're in a module) or a new, empty object.
  runtime = global.regeneratorRuntime = inModule ? module.exports : {};

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  runtime.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  runtime.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  runtime.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  runtime.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return Promise.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return Promise.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration. If the Promise is rejected, however, the
          // result for this iteration will be rejected with the same
          // reason. Note that rejections of yielded Promises are not
          // thrown back into the generator function, as is the case
          // when an awaited Promise is rejected. This difference in
          // behavior between yield and await is important, because it
          // allows the consumer to decide what to do with the yielded
          // rejection (swallow it and continue, manually .throw it back
          // into the generator, abandon iteration, whatever). With
          // await, by contrast, there is no opportunity to examine the
          // rejection reason outside the generator function, so the
          // only option is to throw it from the await expression, and
          // let the generator function handle the exception.
          result.value = unwrapped;
          resolve(result);
        }, reject);
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new Promise(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  runtime.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  runtime.async = function(innerFn, outerFn, self, tryLocsList) {
    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList)
    );

    return runtime.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        if (delegate.iterator.return) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  runtime.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  runtime.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };
})(
  // In sloppy mode, unbound `this` refers to the global object, fallback to
  // Function constructor if we're in global strict mode. That is sadly a form
  // of indirect eval which violates Content Security Policy.
  (function() { return this })() || Function("return this")()
);


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _promise = __webpack_require__(61);

var _promise2 = _interopRequireDefault(_promise);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (fn) {
  return function () {
    var gen = fn.apply(this, arguments);
    return new _promise2.default(function (resolve, reject) {
      function step(key, arg) {
        try {
          var info = gen[key](arg);
          var value = info.value;
        } catch (error) {
          reject(error);
          return;
        }

        if (info.done) {
          resolve(value);
        } else {
          return _promise2.default.resolve(value).then(function (value) {
            step("next", value);
          }, function (err) {
            step("throw", err);
          });
        }
      }

      return step("next");
    });
  };
};

/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(60);
__webpack_require__(20);
__webpack_require__(25);
__webpack_require__(109);
__webpack_require__(117);
__webpack_require__(118);
module.exports = __webpack_require__(0).Promise;


/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(18);
var global = __webpack_require__(2);
var ctx = __webpack_require__(11);
var classof = __webpack_require__(38);
var $export = __webpack_require__(1);
var isObject = __webpack_require__(7);
var aFunction = __webpack_require__(21);
var anInstance = __webpack_require__(110);
var forOf = __webpack_require__(111);
var speciesConstructor = __webpack_require__(62);
var task = __webpack_require__(63).set;
var microtask = __webpack_require__(113)();
var newPromiseCapabilityModule = __webpack_require__(42);
var perform = __webpack_require__(64);
var userAgent = __webpack_require__(114);
var promiseResolve = __webpack_require__(65);
var PROMISE = 'Promise';
var TypeError = global.TypeError;
var process = global.process;
var versions = process && process.versions;
var v8 = versions && versions.v8 || '';
var $Promise = global[PROMISE];
var isNode = classof(process) == 'process';
var empty = function () { /* empty */ };
var Internal, newGenericPromiseCapability, OwnPromiseCapability, Wrapper;
var newPromiseCapability = newGenericPromiseCapability = newPromiseCapabilityModule.f;

var USE_NATIVE = !!function () {
  try {
    // correct subclassing with @@species support
    var promise = $Promise.resolve(1);
    var FakePromise = (promise.constructor = {})[__webpack_require__(3)('species')] = function (exec) {
      exec(empty, empty);
    };
    // unhandled rejections tracking support, NodeJS Promise without it fails @@species test
    return (isNode || typeof PromiseRejectionEvent == 'function')
      && promise.then(empty) instanceof FakePromise
      // v8 6.6 (Node 10 and Chrome 66) have a bug with resolving custom thenables
      // https://bugs.chromium.org/p/chromium/issues/detail?id=830565
      // we can't detect it synchronously, so just check versions
      && v8.indexOf('6.6') !== 0
      && userAgent.indexOf('Chrome/66') === -1;
  } catch (e) { /* empty */ }
}();

// helpers
var isThenable = function (it) {
  var then;
  return isObject(it) && typeof (then = it.then) == 'function' ? then : false;
};
var notify = function (promise, isReject) {
  if (promise._n) return;
  promise._n = true;
  var chain = promise._c;
  microtask(function () {
    var value = promise._v;
    var ok = promise._s == 1;
    var i = 0;
    var run = function (reaction) {
      var handler = ok ? reaction.ok : reaction.fail;
      var resolve = reaction.resolve;
      var reject = reaction.reject;
      var domain = reaction.domain;
      var result, then, exited;
      try {
        if (handler) {
          if (!ok) {
            if (promise._h == 2) onHandleUnhandled(promise);
            promise._h = 1;
          }
          if (handler === true) result = value;
          else {
            if (domain) domain.enter();
            result = handler(value); // may throw
            if (domain) {
              domain.exit();
              exited = true;
            }
          }
          if (result === reaction.promise) {
            reject(TypeError('Promise-chain cycle'));
          } else if (then = isThenable(result)) {
            then.call(result, resolve, reject);
          } else resolve(result);
        } else reject(value);
      } catch (e) {
        if (domain && !exited) domain.exit();
        reject(e);
      }
    };
    while (chain.length > i) run(chain[i++]); // variable length - can't use forEach
    promise._c = [];
    promise._n = false;
    if (isReject && !promise._h) onUnhandled(promise);
  });
};
var onUnhandled = function (promise) {
  task.call(global, function () {
    var value = promise._v;
    var unhandled = isUnhandled(promise);
    var result, handler, console;
    if (unhandled) {
      result = perform(function () {
        if (isNode) {
          process.emit('unhandledRejection', value, promise);
        } else if (handler = global.onunhandledrejection) {
          handler({ promise: promise, reason: value });
        } else if ((console = global.console) && console.error) {
          console.error('Unhandled promise rejection', value);
        }
      });
      // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should
      promise._h = isNode || isUnhandled(promise) ? 2 : 1;
    } promise._a = undefined;
    if (unhandled && result.e) throw result.v;
  });
};
var isUnhandled = function (promise) {
  return promise._h !== 1 && (promise._a || promise._c).length === 0;
};
var onHandleUnhandled = function (promise) {
  task.call(global, function () {
    var handler;
    if (isNode) {
      process.emit('rejectionHandled', promise);
    } else if (handler = global.onrejectionhandled) {
      handler({ promise: promise, reason: promise._v });
    }
  });
};
var $reject = function (value) {
  var promise = this;
  if (promise._d) return;
  promise._d = true;
  promise = promise._w || promise; // unwrap
  promise._v = value;
  promise._s = 2;
  if (!promise._a) promise._a = promise._c.slice();
  notify(promise, true);
};
var $resolve = function (value) {
  var promise = this;
  var then;
  if (promise._d) return;
  promise._d = true;
  promise = promise._w || promise; // unwrap
  try {
    if (promise === value) throw TypeError("Promise can't be resolved itself");
    if (then = isThenable(value)) {
      microtask(function () {
        var wrapper = { _w: promise, _d: false }; // wrap
        try {
          then.call(value, ctx($resolve, wrapper, 1), ctx($reject, wrapper, 1));
        } catch (e) {
          $reject.call(wrapper, e);
        }
      });
    } else {
      promise._v = value;
      promise._s = 1;
      notify(promise, false);
    }
  } catch (e) {
    $reject.call({ _w: promise, _d: false }, e); // wrap
  }
};

// constructor polyfill
if (!USE_NATIVE) {
  // 25.4.3.1 Promise(executor)
  $Promise = function Promise(executor) {
    anInstance(this, $Promise, PROMISE, '_h');
    aFunction(executor);
    Internal.call(this);
    try {
      executor(ctx($resolve, this, 1), ctx($reject, this, 1));
    } catch (err) {
      $reject.call(this, err);
    }
  };
  // eslint-disable-next-line no-unused-vars
  Internal = function Promise(executor) {
    this._c = [];             // <- awaiting reactions
    this._a = undefined;      // <- checked in isUnhandled reactions
    this._s = 0;              // <- state
    this._d = false;          // <- done
    this._v = undefined;      // <- value
    this._h = 0;              // <- rejection state, 0 - default, 1 - handled, 2 - unhandled
    this._n = false;          // <- notify
  };
  Internal.prototype = __webpack_require__(115)($Promise.prototype, {
    // 25.4.5.3 Promise.prototype.then(onFulfilled, onRejected)
    then: function then(onFulfilled, onRejected) {
      var reaction = newPromiseCapability(speciesConstructor(this, $Promise));
      reaction.ok = typeof onFulfilled == 'function' ? onFulfilled : true;
      reaction.fail = typeof onRejected == 'function' && onRejected;
      reaction.domain = isNode ? process.domain : undefined;
      this._c.push(reaction);
      if (this._a) this._a.push(reaction);
      if (this._s) notify(this, false);
      return reaction.promise;
    },
    // 25.4.5.1 Promise.prototype.catch(onRejected)
    'catch': function (onRejected) {
      return this.then(undefined, onRejected);
    }
  });
  OwnPromiseCapability = function () {
    var promise = new Internal();
    this.promise = promise;
    this.resolve = ctx($resolve, promise, 1);
    this.reject = ctx($reject, promise, 1);
  };
  newPromiseCapabilityModule.f = newPromiseCapability = function (C) {
    return C === $Promise || C === Wrapper
      ? new OwnPromiseCapability(C)
      : newGenericPromiseCapability(C);
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Promise: $Promise });
__webpack_require__(23)($Promise, PROMISE);
__webpack_require__(116)(PROMISE);
Wrapper = __webpack_require__(0)[PROMISE];

// statics
$export($export.S + $export.F * !USE_NATIVE, PROMISE, {
  // 25.4.4.5 Promise.reject(r)
  reject: function reject(r) {
    var capability = newPromiseCapability(this);
    var $$reject = capability.reject;
    $$reject(r);
    return capability.promise;
  }
});
$export($export.S + $export.F * (LIBRARY || !USE_NATIVE), PROMISE, {
  // 25.4.4.6 Promise.resolve(x)
  resolve: function resolve(x) {
    return promiseResolve(LIBRARY && this === Wrapper ? $Promise : this, x);
  }
});
$export($export.S + $export.F * !(USE_NATIVE && __webpack_require__(57)(function (iter) {
  $Promise.all(iter)['catch'](empty);
})), PROMISE, {
  // 25.4.4.1 Promise.all(iterable)
  all: function all(iterable) {
    var C = this;
    var capability = newPromiseCapability(C);
    var resolve = capability.resolve;
    var reject = capability.reject;
    var result = perform(function () {
      var values = [];
      var index = 0;
      var remaining = 1;
      forOf(iterable, false, function (promise) {
        var $index = index++;
        var alreadyCalled = false;
        values.push(undefined);
        remaining++;
        C.resolve(promise).then(function (value) {
          if (alreadyCalled) return;
          alreadyCalled = true;
          values[$index] = value;
          --remaining || resolve(values);
        }, reject);
      });
      --remaining || resolve(values);
    });
    if (result.e) reject(result.v);
    return capability.promise;
  },
  // 25.4.4.4 Promise.race(iterable)
  race: function race(iterable) {
    var C = this;
    var capability = newPromiseCapability(C);
    var reject = capability.reject;
    var result = perform(function () {
      forOf(iterable, false, function (promise) {
        C.resolve(promise).then(capability.resolve, reject);
      });
    });
    if (result.e) reject(result.v);
    return capability.promise;
  }
});


/***/ }),
/* 110 */
/***/ (function(module, exports) {

module.exports = function (it, Constructor, name, forbiddenField) {
  if (!(it instanceof Constructor) || (forbiddenField !== undefined && forbiddenField in it)) {
    throw TypeError(name + ': incorrect invocation!');
  } return it;
};


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

var ctx = __webpack_require__(11);
var call = __webpack_require__(55);
var isArrayIter = __webpack_require__(56);
var anObject = __webpack_require__(4);
var toLength = __webpack_require__(30);
var getIterFn = __webpack_require__(37);
var BREAK = {};
var RETURN = {};
var exports = module.exports = function (iterable, entries, fn, that, ITERATOR) {
  var iterFn = ITERATOR ? function () { return iterable; } : getIterFn(iterable);
  var f = ctx(fn, that, entries ? 2 : 1);
  var index = 0;
  var length, step, iterator, result;
  if (typeof iterFn != 'function') throw TypeError(iterable + ' is not iterable!');
  // fast case for arrays with default iterator
  if (isArrayIter(iterFn)) for (length = toLength(iterable.length); length > index; index++) {
    result = entries ? f(anObject(step = iterable[index])[0], step[1]) : f(iterable[index]);
    if (result === BREAK || result === RETURN) return result;
  } else for (iterator = iterFn.call(iterable); !(step = iterator.next()).done;) {
    result = call(iterator, f, step.value, entries);
    if (result === BREAK || result === RETURN) return result;
  }
};
exports.BREAK = BREAK;
exports.RETURN = RETURN;


/***/ }),
/* 112 */
/***/ (function(module, exports) {

// fast apply, http://jsperf.lnkit.com/fast-apply/5
module.exports = function (fn, args, that) {
  var un = that === undefined;
  switch (args.length) {
    case 0: return un ? fn()
                      : fn.call(that);
    case 1: return un ? fn(args[0])
                      : fn.call(that, args[0]);
    case 2: return un ? fn(args[0], args[1])
                      : fn.call(that, args[0], args[1]);
    case 3: return un ? fn(args[0], args[1], args[2])
                      : fn.call(that, args[0], args[1], args[2]);
    case 4: return un ? fn(args[0], args[1], args[2], args[3])
                      : fn.call(that, args[0], args[1], args[2], args[3]);
  } return fn.apply(that, args);
};


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(2);
var macrotask = __webpack_require__(63).set;
var Observer = global.MutationObserver || global.WebKitMutationObserver;
var process = global.process;
var Promise = global.Promise;
var isNode = __webpack_require__(17)(process) == 'process';

module.exports = function () {
  var head, last, notify;

  var flush = function () {
    var parent, fn;
    if (isNode && (parent = process.domain)) parent.exit();
    while (head) {
      fn = head.fn;
      head = head.next;
      try {
        fn();
      } catch (e) {
        if (head) notify();
        else last = undefined;
        throw e;
      }
    } last = undefined;
    if (parent) parent.enter();
  };

  // Node.js
  if (isNode) {
    notify = function () {
      process.nextTick(flush);
    };
  // browsers with MutationObserver, except iOS Safari - https://github.com/zloirock/core-js/issues/339
  } else if (Observer && !(global.navigator && global.navigator.standalone)) {
    var toggle = true;
    var node = document.createTextNode('');
    new Observer(flush).observe(node, { characterData: true }); // eslint-disable-line no-new
    notify = function () {
      node.data = toggle = !toggle;
    };
  // environments with maybe non-completely correct, but existent Promise
  } else if (Promise && Promise.resolve) {
    // Promise.resolve without an argument throws an error in LG WebOS 2
    var promise = Promise.resolve(undefined);
    notify = function () {
      promise.then(flush);
    };
  // for other environments - macrotask based on:
  // - setImmediate
  // - MessageChannel
  // - window.postMessag
  // - onreadystatechange
  // - setTimeout
  } else {
    notify = function () {
      // strange IE + webpack dev server bug - use .call(global)
      macrotask.call(global, flush);
    };
  }

  return function (fn) {
    var task = { fn: fn, next: undefined };
    if (last) last.next = task;
    if (!head) {
      head = task;
      notify();
    } last = task;
  };
};


/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(2);
var navigator = global.navigator;

module.exports = navigator && navigator.userAgent || '';


/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

var hide = __webpack_require__(9);
module.exports = function (target, src, safe) {
  for (var key in src) {
    if (safe && target[key]) target[key] = src[key];
    else hide(target, key, src[key]);
  } return target;
};


/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__(2);
var core = __webpack_require__(0);
var dP = __webpack_require__(6);
var DESCRIPTORS = __webpack_require__(5);
var SPECIES = __webpack_require__(3)('species');

module.exports = function (KEY) {
  var C = typeof core[KEY] == 'function' ? core[KEY] : global[KEY];
  if (DESCRIPTORS && C && !C[SPECIES]) dP.f(C, SPECIES, {
    configurable: true,
    get: function () { return this; }
  });
};


/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// https://github.com/tc39/proposal-promise-finally

var $export = __webpack_require__(1);
var core = __webpack_require__(0);
var global = __webpack_require__(2);
var speciesConstructor = __webpack_require__(62);
var promiseResolve = __webpack_require__(65);

$export($export.P + $export.R, 'Promise', { 'finally': function (onFinally) {
  var C = speciesConstructor(this, core.Promise || global.Promise);
  var isFunction = typeof onFinally == 'function';
  return this.then(
    isFunction ? function (x) {
      return promiseResolve(C, onFinally()).then(function () { return x; });
    } : onFinally,
    isFunction ? function (e) {
      return promiseResolve(C, onFinally()).then(function () { throw e; });
    } : onFinally
  );
} });


/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// https://github.com/tc39/proposal-promise-try
var $export = __webpack_require__(1);
var newPromiseCapability = __webpack_require__(42);
var perform = __webpack_require__(64);

$export($export.S, 'Promise', { 'try': function (callbackfn) {
  var promiseCapability = newPromiseCapability.f(this);
  var result = perform(callbackfn);
  (result.e ? promiseCapability.reject : promiseCapability.resolve)(result.v);
  return promiseCapability.promise;
} });


/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(120);
module.exports = __webpack_require__(0).Object.keys;


/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 Object.keys(O)
var toObject = __webpack_require__(14);
var $keys = __webpack_require__(13);

__webpack_require__(43)('keys', function () {
  return function keys(it) {
    return $keys(toObject(it));
  };
});


/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(122);
var $Object = __webpack_require__(0).Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(1);
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(5), 'Object', { defineProperty: __webpack_require__(6).f });


/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(124), __esModule: true };

/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(25);
__webpack_require__(20);
module.exports = __webpack_require__(125);


/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__(38);
var ITERATOR = __webpack_require__(3)('iterator');
var Iterators = __webpack_require__(15);
module.exports = __webpack_require__(0).isIterable = function (it) {
  var O = Object(it);
  return O[ITERATOR] !== undefined
    || '@@iterator' in O
    // eslint-disable-next-line no-prototype-builtins
    || Iterators.hasOwnProperty(classof(O));
};


/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(127), __esModule: true };

/***/ }),
/* 127 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(25);
__webpack_require__(20);
module.exports = __webpack_require__(128);


/***/ }),
/* 128 */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(4);
var get = __webpack_require__(37);
module.exports = __webpack_require__(0).getIterator = function (it) {
  var iterFn = get(it);
  if (typeof iterFn != 'function') throw TypeError(it + ' is not iterable!');
  return anObject(iterFn.call(it));
};


/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(130);
module.exports = __webpack_require__(0).Object.entries;


/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

// https://github.com/tc39/proposal-object-values-entries
var $export = __webpack_require__(1);
var $entries = __webpack_require__(58)(true);

$export($export.S, 'Object', {
  entries: function entries(it) {
    return $entries(it);
  }
});


/***/ }),
/* 131 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(132);
module.exports = __webpack_require__(0).Object.getPrototypeOf;


/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 Object.getPrototypeOf(O)
var toObject = __webpack_require__(14);
var $getPrototypeOf = __webpack_require__(54);

__webpack_require__(43)('getPrototypeOf', function () {
  return function getPrototypeOf(it) {
    return $getPrototypeOf(toObject(it));
  };
});


/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(67);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof2 = __webpack_require__(24);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && ((typeof call === "undefined" ? "undefined" : (0, _typeof3.default)(call)) === "object" || typeof call === "function") ? call : self;
};

/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getPrototypeOf = __webpack_require__(70);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _getOwnPropertyDescriptor = __webpack_require__(137);

var _getOwnPropertyDescriptor2 = _interopRequireDefault(_getOwnPropertyDescriptor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function get(object, property, receiver) {
  if (object === null) object = Function.prototype;
  var desc = (0, _getOwnPropertyDescriptor2.default)(object, property);

  if (desc === undefined) {
    var parent = (0, _getPrototypeOf2.default)(object);

    if (parent === null) {
      return undefined;
    } else {
      return get(parent, property, receiver);
    }
  } else if ("value" in desc) {
    return desc.value;
  } else {
    var getter = desc.get;

    if (getter === undefined) {
      return undefined;
    }

    return getter.call(receiver);
  }
};

/***/ }),
/* 137 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(138), __esModule: true };

/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(139);
var $Object = __webpack_require__(0).Object;
module.exports = function getOwnPropertyDescriptor(it, key) {
  return $Object.getOwnPropertyDescriptor(it, key);
};


/***/ }),
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
var toIObject = __webpack_require__(8);
var $getOwnPropertyDescriptor = __webpack_require__(41).f;

__webpack_require__(43)('getOwnPropertyDescriptor', function () {
  return function getOwnPropertyDescriptor(it, key) {
    return $getOwnPropertyDescriptor(toIObject(it), key);
  };
});


/***/ }),
/* 140 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setPrototypeOf = __webpack_require__(141);

var _setPrototypeOf2 = _interopRequireDefault(_setPrototypeOf);

var _create = __webpack_require__(145);

var _create2 = _interopRequireDefault(_create);

var _typeof2 = __webpack_require__(24);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : (0, _typeof3.default)(superClass)));
  }

  subClass.prototype = (0, _create2.default)(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf2.default ? (0, _setPrototypeOf2.default)(subClass, superClass) : subClass.__proto__ = superClass;
};

/***/ }),
/* 141 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(142), __esModule: true };

/***/ }),
/* 142 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(143);
module.exports = __webpack_require__(0).Object.setPrototypeOf;


/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $export = __webpack_require__(1);
$export($export.S, 'Object', { setPrototypeOf: __webpack_require__(144).set });


/***/ }),
/* 144 */
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__(7);
var anObject = __webpack_require__(4);
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__(11)(Function.call, __webpack_require__(41).f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),
/* 145 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(146), __esModule: true };

/***/ }),
/* 146 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(147);
var $Object = __webpack_require__(0).Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};


/***/ }),
/* 147 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(1);
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
$export($export.S, 'Object', { create: __webpack_require__(36) });


/***/ }),
/* 148 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var R = typeof Reflect === 'object' ? Reflect : null
var ReflectApply = R && typeof R.apply === 'function'
  ? R.apply
  : function ReflectApply(target, receiver, args) {
    return Function.prototype.apply.call(target, receiver, args);
  }

var ReflectOwnKeys
if (R && typeof R.ownKeys === 'function') {
  ReflectOwnKeys = R.ownKeys
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target)
      .concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}

function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}

var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
}

function EventEmitter() {
  EventEmitter.init.call(this);
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;

function checkListener(listener) {
  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }
}

Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
  enumerable: true,
  get: function() {
    return defaultMaxListeners;
  },
  set: function(arg) {
    if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
    }
    defaultMaxListeners = arg;
  }
});

EventEmitter.init = function() {

  if (this._events === undefined ||
      this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
  }
  this._maxListeners = n;
  return this;
};

function _getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return _getMaxListeners(this);
};

EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = (type === 'error');

  var events = this._events;
  if (events !== undefined)
    doError = (doError && events.error === undefined);
  else if (!doError)
    return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0)
      er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];

  if (handler === undefined)
    return false;

  if (typeof handler === 'function') {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      ReflectApply(listeners[i], this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  checkListener(listener);

  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] =
        prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = _getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error('Possible EventEmitter memory leak detected. ' +
                          existing.length + ' ' + String(type) + ' listeners ' +
                          'added. Use emitter.setMaxListeners() to ' +
                          'increase limit');
      w.name = 'MaxListenersExceededWarning';
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }

  return target;
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function onceWrapper() {
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    if (arguments.length === 0)
      return this.listener.call(this.target);
    return this.listener.apply(this.target, arguments);
  }
}

function _onceWrap(target, type, listener) {
  var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}

EventEmitter.prototype.once = function once(type, listener) {
  checkListener(listener);
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      checkListener(listener);
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      checkListener(listener);

      events = this._events;
      if (events === undefined)
        return this;

      list = events[type];
      if (list === undefined)
        return this;

      if (list === listener || list.listener === listener) {
        if (--this._eventsCount === 0)
          this._events = Object.create(null);
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length - 1; i >= 0; i--) {
          if (list[i] === listener || list[i].listener === listener) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (position === 0)
          list.shift();
        else {
          spliceOne(list, position);
        }

        if (list.length === 1)
          events[type] = list[0];

        if (events.removeListener !== undefined)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events, i;

      events = this._events;
      if (events === undefined)
        return this;

      // not listening for removeListener, no need to emit
      if (events.removeListener === undefined) {
        if (arguments.length === 0) {
          this._events = Object.create(null);
          this._eventsCount = 0;
        } else if (events[type] !== undefined) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        var key;
        for (i = 0; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = Object.create(null);
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners !== undefined) {
        // LIFO order
        for (i = listeners.length - 1; i >= 0; i--) {
          this.removeListener(type, listeners[i]);
        }
      }

      return this;
    };

function _listeners(target, type, unwrap) {
  var events = target._events;

  if (events === undefined)
    return [];

  var evlistener = events[type];
  if (evlistener === undefined)
    return [];

  if (typeof evlistener === 'function')
    return unwrap ? [evlistener.listener || evlistener] : [evlistener];

  return unwrap ?
    unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}

EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};

EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events !== undefined) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};

function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i)
    copy[i] = arr[i];
  return copy;
}

function spliceOne(list, index) {
  for (; index + 1 < list.length; index++)
    list[index] = list[index + 1];
  list.pop();
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}


/***/ }),
/* 149 */
/***/ (function(module) {

module.exports = JSON.parse("{\"name\":\"rfplayer\",\"version\":\"3.0.0-alpha.8\",\"description\":\"Lecteur Commun Radio France\",\"dependencies\":{\"@babel/runtime\":\"^7.1.5\"},\"main\":\"dist/rfplayer.js\",\"module\":\"module/index.js\",\"devDependencies\":{\"babel-cli\":\"^6.26.0\",\"babel-core\":\"^6.26.0\",\"babel-eslint\":\"^8.0.1\",\"babel-loader\":\"7.1.5\",\"babel-plugin-transform-object-rest-spread\":\"^6.26.0\",\"babel-plugin-transform-runtime\":\"^6.23.0\",\"babel-preset-env\":\"^1.6.0\",\"babel-preset-react\":\"^6.24.1\",\"babel-register\":\"^6.22.0\",\"chai\":\"4.1.2\",\"esdoc\":\"^1.0.3\",\"eslint\":\"^5.4.0\",\"eslint-config-airbnb\":\"^17.1.0\",\"eslint-loader\":\"^2.1.0\",\"eslint-plugin-import\":\"^2.2.0\",\"eslint-plugin-jsx-a11y\":\"^6.0.2\",\"eslint-plugin-react\":\"^7.4.0\",\"file-loader\":\"^2.0.0\",\"jsdom\":\"^12.0.0\",\"lolex\":\"^2.3.1\",\"mocha\":\"^5.2.0\",\"open-browser-webpack-plugin\":\"0.0.5\",\"sinon\":\"^6.1.5\",\"uglifyjs-webpack-plugin\":\"^1.3.0\",\"webpack\":\"^4.17.1\",\"webpack-cli\":\"^3.1.0\",\"webpack-dev-server\":\"^3.1.6\",\"webpack-manifest-plugin\":\"^2.0.3\"},\"peerDependencies\":{\"babel-plugin-transform-object-rest-spread\":\"^6.20.2\"},\"scripts\":{\"test\":\"mocha\",\"clean-release\":\"rm -rf ./dist/**\",\"build\":\"npm run clean-release && webpack --progress --colors --config webpack.config.babel.js\",\"prepublishOnly\":\"babel ./src/*.js -d ./module\",\"lint\":\"eslint src --ignore-pattern 'src/commons/' --ignore-pattern 'src/models/'\"},\"repository\":{\"type\":\"git\",\"url\":\"git@gitlab.dev.dnm.radiofrance.fr/lecteur-commun/rfplayer.git\"},\"author\":\"Radio France\",\"license\":\"CECILL-B\",\"pre-commit\":[\"lint\",\"test\"]}");

/***/ }),
/* 150 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(44);

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends3 = __webpack_require__(68);

var _extends4 = _interopRequireDefault(_extends3);

var _slicedToArray2 = __webpack_require__(45);

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _entries = __webpack_require__(69);

var _entries2 = _interopRequireDefault(_entries);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint no-console: 0 */
var loggify = function loggify(core, handlerCollection) {
  return (0, _entries2.default)(handlerCollection).reduce(function (resultSoFar, _ref) {
    var _ref2 = (0, _slicedToArray3.default)(_ref, 2),
        handlerName = _ref2[0],
        handler = _ref2[1];

    return (0, _extends4.default)({}, resultSoFar, (0, _defineProperty3.default)({}, handlerName, function (adapter) {
      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      core.debug(adapter.name, 'Handling ' + handlerName + ' event');
      if (adapter[handlerName] && typeof adapter[handlerName] === 'function') {
        return adapter[handlerName].apply(adapter, args);
      }
      return handler.apply(undefined, [adapter].concat(args));
    }));
  }, {});
};

var _calcCurrentTime = function _calcCurrentTime(time, core) {
  var _core$BROADCAST_TYPE_;

  var media = core.getMedia();
  if (!media) {
    return 0;
  }

  var _core$getCurrentSourc = core.getCurrentSource(),
      broadcastType = _core$getCurrentSourc.broadcastType;

  var _core$getStatus = core.getStatus(),
      timeshiftDate = _core$getStatus.timeshiftDate;

  var startTime = media.startTime || 0;

  return (_core$BROADCAST_TYPE_ = {}, (0, _defineProperty3.default)(_core$BROADCAST_TYPE_, core.BROADCAST_TYPE_REPLAY, function (t) {
    return t;
  }), (0, _defineProperty3.default)(_core$BROADCAST_TYPE_, core.BROADCAST_TYPE_LIVE, function () {
    return new Date().getTime() / 1000 - startTime;
  }), (0, _defineProperty3.default)(_core$BROADCAST_TYPE_, core.BROADCAST_TYPE_TIMESHIFTING, function (t) {
    return timeshiftDate + t - startTime;
  }), _core$BROADCAST_TYPE_)[broadcastType](time);
};

var _checkFullscreen = function _checkFullscreen(core) {
  var fullscreenValues = [document.fullScreen, document.mozFullScreen, document.webkitIsFullScreen];

  var result = fullscreenValues.some(function (value) {
    return value;
  });
  if (fullscreenValues.every(function (value) {
    return value === undefined;
  })) {
    // if document.[prefix]fullscreen isn't supported
    result = core.getStatus().fullscreen;
  }
  return result;
};

var getEventHandlers = function getEventHandlers(core) {
  return {
    onEnded: function onEnded() {
      core.setStatus({ state: core.STATE_STOPPED });
    },
    onError: function onError(adapter, event) {
      if (adapter._error) {
        return adapter._error(event);
      }

      return core.emit(core.ERROR_EVENT, {
        adapter: adapter,
        event: event
      });
    },
    onDurationchange: function onDurationchange(adapter, event) {
      var duration = event.target.duration;

      if (duration) {
        core.setStatus({ parsedDuration: duration });
      }
    },
    onFullscreenchange: function onFullscreenchange(adapter) {
      var isFullscreen = _checkFullscreen(core);
      var fullscreenElement = core.getFullscreenElement();
      if (isFullscreen) {
        fullscreenElement.classList.add('fullscreen');
        core.emit(core.FULLSCREEN_ACTIVATED, adapter);
      } else {
        fullscreenElement.classList.remove('fullscreen');
        core.emit(core.FULLSCREEN_DEACTIVATED, adapter);
      }
    },
    onLoadedmetadata: function onLoadedmetadata(adapter, event) {
      var duration = event.target.duration;

      if (duration) {
        core.setDuration(duration);
      }
      core.emit(core.METADATA_IS_LOADED, adapter);
    },
    onPause: function onPause(adapter) {
      var _core$getStatus2 = core.getStatus(),
          isStopping = _core$getStatus2.isStopping;

      core.debug('core', 'got pause event');
      if (isStopping) {
        core.debug('core', 'interpreting it as stop event');
        core.setStatus({
          isStopping: false,
          state: core.STATE_STOPPED
        });
        core.emit(core.STATE_STOPPED, adapter);
      } else if (!core.isStopped()) {
        core.setStatus({
          state: core.STATE_PAUSED
        });
        core.emit(core.STATE_PAUSED, adapter);
      } else {
        core.debug('core', 'ignored it');
      }
    },
    onPlay: function onPlay(adapter) {
      core.emit(core.STATE_PLAY, adapter);
    },
    onPlaying: function onPlaying(adapter, event, silent) {
      var _core$getStatus3 = core.getStatus(),
          state = _core$getStatus3.state,
          volume = _core$getStatus3.volume;

      if (state !== core.STATE_PLAYING) {
        core.setStatus({
          state: core.STATE_PLAYING
        });
      }
      adapter.setVolume(volume);
      if (!silent) {
        core.emit(core.STATE_PLAYING, adapter);
      }
    },
    onProgress: function onProgress() {},
    onReady: function onReady(adapter) {
      if (core.getStatus().state === core.STATE_UNREADY) {
        core.setStatus({ state: core.STATE_READY });
      }
      if (adapter.start) {
        adapter.start();
      }
      core.emit(core.STATE_READY, adapter);
    },
    onStalled: function onStalled(adapter) {
      if (core.getStatus().state !== core.STATE_READY) {
        core.setStatus({ state: core.STALLED });
        core.emit(core.STALLED, adapter);
      }
    },
    onTimeupdate: function onTimeupdate(adapter) {
      var _core$getStatus4 = core.getStatus(),
          state = _core$getStatus4.state;

      if (state === core.WAITING) {
        core.setStatus({
          state: core.STATE_PLAYING
        });
      }
      var time = adapter.getCurrentTime();
      var currentTime = _calcCurrentTime(time, core);
      core.setStatus({ currentTime: currentTime });

      if (core._checkEndReached(currentTime)) {
        core.emit(core.END_REACHED, adapter);
      }

      core.emit(core.TIME_IS_UPDATED, adapter);
    },
    onVolumechange: function onVolumechange(adapter /* , event */) {
      var status = core.getStatus();

      var listEvents = [core.VOLUME_UPDATED];
      var newMuteStatus = adapter.isMuted();
      if (status.muted !== newMuteStatus) {
        listEvents.push(newMuteStatus ? core.IS_MUTED : core.IS_UNMUTED);
      }
      listEvents.forEach(function (event) {
        return core.emit(event, adapter);
      });
      core.setStatus({
        muted: newMuteStatus,
        volume: adapter.getVolume()
      });
    },
    onWaiting: function onWaiting(adapter) {
      core.emit(core.WAITING, adapter);
    },
    onSeeked: function onSeeked(adapter) {
      core.setStatus({ seekingTo: null });

      if (!core.isStopped()) {
        core.emit(core.TIME_SEEK, adapter);
      }
    }
  };
};

exports.default = function (core) {
  return loggify(core, getEventHandlers(core));
};

/***/ }),
/* 151 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.chainConstraints = exports.findSourceIndex = exports.sortTimeRange = exports.getComputedSourceMetadata = exports.isMimeTypeSupported = exports.getComputedExtensionAndMimeType = exports.mergeDeep = exports.isObject = exports.getMimeTypeFromExtension = exports.getExtension = exports.log = exports.error = exports.OUTPUT_TYPE_TO_MIME_TYPES = exports.OUTPUT_TYPE_TO_EXTENSIONS = undefined;

var _isFinite = __webpack_require__(152);

var _isFinite2 = _interopRequireDefault(_isFinite);

var _isNan = __webpack_require__(155);

var _isNan2 = _interopRequireDefault(_isNan);

var _assign = __webpack_require__(26);

var _assign2 = _interopRequireDefault(_assign);

var _typeof2 = __webpack_require__(24);

var _typeof3 = _interopRequireDefault(_typeof2);

var _slicedToArray2 = __webpack_require__(45);

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _keys = __webpack_require__(66);

var _keys2 = _interopRequireDefault(_keys);

var _defineProperty2 = __webpack_require__(44);

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _OUTPUT_TYPE_TO_EXTEN, _OUTPUT_TYPE_TO_MIME_; /* eslint no-console: 0 */


exports.randomUid = randomUid;
exports.versionIsGreater = versionIsGreater;
exports.startBefore = startBefore;
exports.endBefore = endBefore;

var _constants = __webpack_require__(46);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Caution : only put lower-case letters here
var OUTPUT_TYPE_TO_EXTENSIONS = exports.OUTPUT_TYPE_TO_EXTENSIONS = (_OUTPUT_TYPE_TO_EXTEN = {}, (0, _defineProperty3.default)(_OUTPUT_TYPE_TO_EXTEN, _constants.OUTPUT_TYPE_VIDEO, ['mpd', 'm4v']), (0, _defineProperty3.default)(_OUTPUT_TYPE_TO_EXTEN, _constants.OUTPUT_TYPE_AUDIO, ['mp3', 'm4a', 'aac', 'hls', 'm3u8', 'm3u8a']), _OUTPUT_TYPE_TO_EXTEN);

var OUTPUT_TYPE_TO_MIME_TYPES = exports.OUTPUT_TYPE_TO_MIME_TYPES = (_OUTPUT_TYPE_TO_MIME_ = {}, (0, _defineProperty3.default)(_OUTPUT_TYPE_TO_MIME_, _constants.OUTPUT_TYPE_VIDEO, ['video/mp4', 'application/dash']), (0, _defineProperty3.default)(_OUTPUT_TYPE_TO_MIME_, _constants.OUTPUT_TYPE_AUDIO, ['application/vnd.apple.mpegurl', 'audio/aac', 'audio/mpeg', 'audio/mp4']), _OUTPUT_TYPE_TO_MIME_);

var OUTPUT_TYPE_TO_SOURCE_TYPES = (0, _defineProperty3.default)({}, _constants.OUTPUT_TYPE_VIDEO, ['dailymotion']);

var EXTENSION_TO_MIME_TYPE = {
  mpd: 'application/dash',
  m3u8: 'application/vnd.apple.mpegurl',
  hls: 'application/vnd.apple.mpegurl',
  mp3: 'audio/mpeg',
  m4a: 'audio/aac',
  aac: 'audio/aac',
  m4v: 'video/mp4',
  mp4: 'video/mp4'
};

function randomUid() {
  return (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
}

var _console = console,
    error = _console.error,
    log = _console.info;

/**
 * return extension of url
 * @param {string} url url
 * @return {string} extension
 */

exports.error = error;
exports.log = log;
var getExtension = exports.getExtension = function getExtension(url) {
  var regexPattern = /\/([^/?]*)\.([^/?]+)(\??[^/]*)$/;
  var extension = url.match(regexPattern) && url.toLowerCase().trim().match(regexPattern)[2];

  return extension || '';
};

var getMimeTypeFromExtension = exports.getMimeTypeFromExtension = function getMimeTypeFromExtension(extension) {
  return EXTENSION_TO_MIME_TYPE[extension];
};

var getOutputTypeBySourceType = function getOutputTypeBySourceType(sourceType) {
  return (0, _keys2.default)(OUTPUT_TYPE_TO_SOURCE_TYPES).find(function (outputType) {
    return OUTPUT_TYPE_TO_SOURCE_TYPES[outputType].find(function (type) {
      return type === sourceType;
    });
  });
};

var getOutputTypeByExtension = function getOutputTypeByExtension(extension) {
  return (0, _keys2.default)(OUTPUT_TYPE_TO_EXTENSIONS).find(function (outputType) {
    return OUTPUT_TYPE_TO_EXTENSIONS[outputType].find(function (ext) {
      return ext === extension;
    });
  });
};

var getOutputTypeByMimeType = function getOutputTypeByMimeType(mimeType) {
  if (!mimeType) {
    return null;
  }

  var _mimeType$split = mimeType.split('/'),
      _mimeType$split2 = (0, _slicedToArray3.default)(_mimeType$split, 1),
      mimeTypeLeftPart = _mimeType$split2[0];

  var knownOutputTypes = (0, _keys2.default)(OUTPUT_TYPE_TO_EXTENSIONS);
  return knownOutputTypes.find(function (outputType) {
    return outputType === mimeTypeLeftPart;
  }) || (0, _keys2.default)(OUTPUT_TYPE_TO_MIME_TYPES).find(function (outputType) {
    return OUTPUT_TYPE_TO_MIME_TYPES[outputType].find(function (type) {
      return type === mimeType;
    });
  });
};

var getOutputType = function getOutputType(extension, mimeType, sourceType) {
  return getOutputTypeBySourceType(sourceType) || getOutputTypeByMimeType(mimeType) || getOutputTypeByExtension(extension) || _constants.OUTPUT_TYPE_VIDEO;
};

/**
 * Simple is object check.
 * @param item
 * @returns {boolean}
 */
var isObject = exports.isObject = function isObject(item) {
  return item && (typeof item === 'undefined' ? 'undefined' : (0, _typeof3.default)(item)) === 'object' && !Array.isArray(item) && item !== null;
};

/**
 * Deep merge two objects.
 * @param target
 * @param source
 */
var mergeDeep = exports.mergeDeep = function mergeDeep(target, source) {
  if (isObject(target) && isObject(source)) {
    (0, _keys2.default)(source).forEach(function (key) {
      if (isObject(source[key])) {
        if (!target[key]) (0, _assign2.default)(target, (0, _defineProperty3.default)({}, key, {}));
        mergeDeep(target[key], source[key]);
      } else {
        (0, _assign2.default)(target, (0, _defineProperty3.default)({}, key, source[key]));
      }
    });
  }
  return target;
};

var isMimeType = function isMimeType(formatName) {
  return (/^.+\/.+$/.test(formatName)
  );
};

/**
 * return the computed extension and mimeType from media source
 * based on source.url and source.format when present
 * @param {object<source>} source source
 * @return {{extension: string, mimeType: string}}
 */
var getComputedExtensionAndMimeType = exports.getComputedExtensionAndMimeType = function getComputedExtensionAndMimeType() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      format = _ref.format,
      _ref$format = _ref.format;

  _ref$format = _ref$format === undefined ? {} : _ref$format;
  var _ref$format$name = _ref$format.name,
      name = _ref$format$name === undefined ? '' : _ref$format$name,
      _ref$format$behavior = _ref$format.behavior,
      behavior = _ref$format$behavior === undefined ? _constants.FORMAT_BEHAVIOUR_FALLBACK : _ref$format$behavior,
      _ref$url = _ref.url,
      url = _ref$url === undefined ? '' : _ref$url;

  var formatName = format && name && name.trim && name.toLowerCase().trim() || '';

  var formatBehavior = format && behavior && behavior.toLowerCase && behavior.trim && behavior.toLowerCase().trim();
  var canFallBack = formatBehavior === _constants.FORMAT_BEHAVIOUR_FALLBACK && !getOutputTypeByExtension(getExtension(url));
  var shouldOverride = formatBehavior === _constants.FORMAT_BEHAVIOUR_OVERRIDE;
  var extension = '';
  var mediaType = void 0;

  if (shouldOverride) {
    extension = formatName;
    mediaType = getOutputTypeByExtension(extension);
  } else if (canFallBack) {
    extension = formatName;
    mediaType = getOutputTypeByExtension(extension);
  } else {
    extension = getExtension(url);
    mediaType = getOutputTypeByExtension(extension);
  }

  return { extension: extension, mimeType: mediaType };
};

var isMimeTypeSupported = exports.isMimeTypeSupported = function isMimeTypeSupported(mimeType) {
  var mediaType = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _constants.OUTPUT_TYPE_AUDIO;
  return !!window.document.createElement(mediaType).canPlayType(mimeType);
};

/**
 * Returns {
 *   outputType: string,
 *   mimeType: string,
 *   extension: string,
 *   fallbackExtension?: string,
 * }
 */

var getComputedSourceMetadata = exports.getComputedSourceMetadata = function getComputedSourceMetadata(_ref2) {
  var _ref2$format = _ref2.format;
  _ref2$format = _ref2$format === undefined ? {} : _ref2$format;
  var formatName = _ref2$format.name,
      _ref2$format$behavior = _ref2$format.behavior,
      behavior = _ref2$format$behavior === undefined ? _constants.FORMAT_BEHAVIOUR_FALLBACK : _ref2$format$behavior,
      url = _ref2.url,
      sourceType = _ref2.type,
      outputType = _ref2.outputType,
      broadcastType = _ref2.broadcastType;

  var extension = getExtension(url);
  var missingExtension = !extension;
  var mimeType = void 0;

  if (behavior === _constants.FORMAT_BEHAVIOUR_OVERRIDE || missingExtension) {
    if (isMimeType(formatName)) {
      mimeType = formatName;
    } else {
      mimeType = getMimeTypeFromExtension(formatName);
      extension = formatName;
    }
  } else {
    mimeType = getMimeTypeFromExtension(extension) || isMimeType(formatName) && formatName;
  }

  var result = {
    outputType: outputType || getOutputType(extension, mimeType, sourceType),
    mimeType: mimeType,
    extension: extension && extension.toLowerCase()
  };
  result.broadcastType = broadcastType || _constants.BROADCAST_TYPE_REPLAY;
  if (behavior === _constants.FORMAT_BEHAVIOUR_FALLBACK && formatName && !isMimeType(formatName)) {
    result.fallbackExtension = formatName.toLowerCase();
    result.fallbackOutputType = getOutputType(result.fallbackExtension);
  }
  return result;
};

/* true if tag1 is greater then tag2 */
function versionIsGreater(tag1, tag2) {
  var versionRegexp = /v?(\d+)\.(\d+)\.(\d+)/;
  // If tag1 is not a version, consider it is not greater
  if (!versionRegexp.test(tag1)) {
    return false;
  }

  // If tag2 is not a version, consider tag1 is greater
  if (!versionRegexp.test(tag2)) {
    return true;
  }

  var _versionRegexp$exec$m = versionRegexp.exec(tag1).map(function (val) {
    return parseInt(val, 10);
  }),
      _versionRegexp$exec$m2 = (0, _slicedToArray3.default)(_versionRegexp$exec$m, 4),
      v1 = _versionRegexp$exec$m2[1],
      v2 = _versionRegexp$exec$m2[2],
      v3 = _versionRegexp$exec$m2[3];

  var _versionRegexp$exec$m3 = versionRegexp.exec(tag2).map(function (val) {
    return parseInt(val, 10);
  }),
      _versionRegexp$exec$m4 = (0, _slicedToArray3.default)(_versionRegexp$exec$m3, 4),
      w1 = _versionRegexp$exec$m4[1],
      w2 = _versionRegexp$exec$m4[2],
      w3 = _versionRegexp$exec$m4[3];

  return v1 > w1 || v1 === w1 && v2 > w2 || v1 === w1 && v2 === w2 && v3 > w3;
}

function isNumeric(n) {
  return !(0, _isNan2.default)(parseFloat(n)) && (0, _isFinite2.default)(n);
}

function startBefore(_ref3, _ref4) {
  var rangeAStart = _ref3.start;
  var rangeBStart = _ref4.start;

  return isNumeric(rangeBStart) && !rangeAStart && !isNumeric(rangeAStart) || isNumeric(rangeAStart) && isNumeric(rangeBStart) && rangeAStart < rangeBStart;
}

function endBefore(_ref5, _ref6) {
  var rangeAEnd = _ref5.end;
  var rangeBEnd = _ref6.end;

  return isNumeric(rangeAEnd) && !rangeBEnd && !isNumeric(rangeBEnd) || isNumeric(rangeAEnd) && isNumeric(rangeBEnd) && rangeAEnd < rangeBEnd;
}

var sortTimeRange = exports.sortTimeRange = function sortTimeRange(rangeA, rangeB) {
  if (startBefore(rangeA, rangeB)) {
    return -1;
  }
  if (startBefore(rangeB, rangeA)) {
    return 1;
  }
  if (endBefore(rangeA, rangeB)) {
    return 1;
  }
  if (endBefore(rangeB, rangeA)) {
    return -1;
  }
  return 0;
};

var findSourceIndex = exports.findSourceIndex = function findSourceIndex(sources, predicate) {
  return sources.findIndex(predicate);
};

var chainConstraints = exports.chainConstraints = function chainConstraints(player) {
  var constraints = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var initialValue = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  return constraints.reduce(function (soFar, currentConstraint) {
    return currentConstraint(soFar, player);
  }, initialValue);
};

exports.default = {
  OUTPUT_TYPE_TO_EXTENSIONS: OUTPUT_TYPE_TO_EXTENSIONS,
  log: log,
  error: error,
  randomUid: randomUid,
  getExtension: getExtension,
  isObject: isObject,
  mergeDeep: mergeDeep,
  getComputedExtensionAndMimeType: getComputedExtensionAndMimeType,
  getComputedSourceMetadata: getComputedSourceMetadata,
  getMimeTypeFromExtension: getMimeTypeFromExtension,
  findSourceIndex: findSourceIndex,
  versionIsGreater: versionIsGreater,
  startBefore: startBefore,
  endBefore: endBefore,
  sortTimeRange: sortTimeRange
};

/***/ }),
/* 152 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(153), __esModule: true };

/***/ }),
/* 153 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(154);
module.exports = __webpack_require__(0).Number.isFinite;


/***/ }),
/* 154 */
/***/ (function(module, exports, __webpack_require__) {

// 20.1.2.2 Number.isFinite(number)
var $export = __webpack_require__(1);
var _isFinite = __webpack_require__(2).isFinite;

$export($export.S, 'Number', {
  isFinite: function isFinite(it) {
    return typeof it == 'number' && _isFinite(it);
  }
});


/***/ }),
/* 155 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(156), __esModule: true };

/***/ }),
/* 156 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(157);
module.exports = __webpack_require__(0).Number.isNaN;


/***/ }),
/* 157 */
/***/ (function(module, exports, __webpack_require__) {

// 20.1.2.4 Number.isNaN(number)
var $export = __webpack_require__(1);

$export($export.S, 'Number', {
  isNaN: function isNaN(number) {
    // eslint-disable-next-line no-self-compare
    return number != number;
  }
});


/***/ }),
/* 158 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isSourceSeekable = isSourceSeekable;
exports.isSourcePauseable = isSourcePauseable;

var _constants = __webpack_require__(46);

function isSourceSeekable(_ref) {
  var _ref$broadcastType = _ref.broadcastType,
      broadcastType = _ref$broadcastType === undefined ? _constants.BROADCAST_TYPE_REPLAY : _ref$broadcastType;

  return [_constants.BROADCAST_TYPE_REPLAY, _constants.BROADCAST_TYPE_TIMESHIFTING].includes(broadcastType);
}

function isSourcePauseable(_ref2) {
  var _ref2$broadcastType = _ref2.broadcastType,
      broadcastType = _ref2$broadcastType === undefined ? _constants.BROADCAST_TYPE_REPLAY : _ref2$broadcastType;

  return [_constants.BROADCAST_TYPE_REPLAY, _constants.BROADCAST_TYPE_TIMESHIFTING].includes(broadcastType);
}

/***/ })
/******/ ]);
});
//# sourceMappingURL=rfplayer.js.map