(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./package.json":
/*!**********************!*\
  !*** ./package.json ***!
  \**********************/
/*! exports provided: name, version, rfplayer-plugin, description, dependencies, devDependencies, scripts, main, module, repository, license, author, pre-commit, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"name\":\"rfplayer-plugin-html5-sound-adapter\",\"version\":\"3.0.0-alpha.4\",\"rfplayer-plugin\":{\"minimumCoreVersion\":\"3.0.0-alpha.7\"},\"description\":\"Plugin qui apporte la lecture audio html5\",\"dependencies\":{},\"devDependencies\":{\"babel-cli\":\"^6.26.0\",\"babel-core\":\"^6.1.2\",\"babel-eslint\":\"^8.0.1\",\"babel-loader\":\"7.1.5\",\"babel-plugin-transform-object-rest-spread\":\"^6.26.0\",\"babel-plugin-transform-runtime\":\"^6.23.0\",\"babel-polyfill\":\"^6.2.0\",\"babel-preset-env\":\"^1.6.0\",\"babel-register\":\"^6.22.0\",\"chai\":\"^4.1.1\",\"eslint\":\"^5.4.0\",\"eslint-config-airbnb\":\"^17.1.0\",\"eslint-loader\":\"^2.0.0\",\"eslint-plugin-import\":\"^2.2.0\",\"eslint-plugin-jsx-a11y\":\"^6.0.2\",\"eslint-plugin-mocha\":\"^5.0.0\",\"eslint-plugin-react\":\"^7.11.1\",\"jsdom\":\"^12.0.0\",\"mocha\":\"^5.0.5\",\"pre-commit\":\"^1.2.2\",\"replace\":\"^1.0.0\",\"semver-regex\":\"^3.1.0\",\"sinon\":\"^6.1.5\",\"webpack\":\"^4.4.0\",\"webpack-cli\":\"^3.1.0\"},\"scripts\":{\"build\":\"webpack --display-modules\",\"prepublishOnly\":\"babel ./src -d ./module && npm run replace\",\"replace\":\"replace \\\"rfplayer/src\\\" \\\"rfplayer/module\\\" ./module -r --include=\\\"*.js\\\"\",\"test\":\"mocha\",\"lint\":\"eslint ./src ./test\"},\"main\":\"src/index.js\",\"module\":\"module/index.js\",\"repository\":{\"type\":\"git\",\"url\":\"git@gitlab.dev.dnm.radiofrance.fr/lecteur-commun/rfplayer-plugin-html5-sound-adapter.git\"},\"license\":\"CECILL-B\",\"author\":\"Radio France\",\"pre-commit\":[\"lint\",\"test\"]}");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Html5SoundAdapter = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /* global document */

var _package = __webpack_require__(/*! ../package.json */ "./package.json");

var _package2 = _interopRequireDefault(_package);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MEDIA_TYPES = {
  // N.B. : only put lower-case letters here
  OUTPUT_TYPE_AUDIO: ['mp3', 'm4a', 'aac', 'm3u8a']
};

var Html5SoundAdapter = exports.Html5SoundAdapter = function () {
  function Html5SoundAdapter(core) {
    _classCallCheck(this, Html5SoundAdapter);

    this._core = core;
    this._mediaElement = null;
  }

  _createClass(Html5SoundAdapter, [{
    key: '_debug',
    value: function _debug() {
      var _core;

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return (_core = this._core).debug.apply(_core, [Html5SoundAdapter.name].concat(args));
    }
  }, {
    key: '_debugPromise',
    value: function _debugPromise() {
      var _core2;

      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      return (_core2 = this._core).debugPromise.apply(_core2, [this.name].concat(args));
    }
  }, {
    key: '_emit',
    value: function _emit(event) {
      var _core3;

      for (var _len3 = arguments.length, args = Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
        args[_key3 - 1] = arguments[_key3];
      }

      (_core3 = this._core).emit.apply(_core3, [event, this].concat(args));
    }
  }, {
    key: 'onProgress',
    value: function onProgress() {
      if (!this._mediaElement) {
        return;
      }

      var _core$getPlaybackData = this._core.getPlaybackData(),
          title = _core$getPlaybackData.title,
          poster = _core$getPlaybackData.poster;

      if (title) {
        this._mediaElement.setAttribute('title', title);
      } else {
        this._mediaElement.removeAttribute('title');
      }
      if (poster) {
        this._mediaElement.setAttribute('poster', poster);
      } else {
        this._mediaElement.removeAttribute('poster');
      }
    }
  }, {
    key: 'getParent',
    value: function getParent() {
      return this._core.getContainer() || document.body;
    }
  }, {
    key: 'getMediaElement',
    value: function getMediaElement() {
      if (!this._mediaElement) {
        this._mediaElement = document.createElement('audio');
        var initOptions = this._core.getInitOptions();
        if (initOptions && initOptions.plugins && initOptions.plugins.Html5SoundAdapter && initOptions.plugins.Html5SoundAdapter.defaultControls) {
          this._mediaElement.setAttribute('controls', '');
        }
        var parent = this.getParent();
        parent.appendChild(this._mediaElement);
      }
      return this._mediaElement;
    }

    /**
     * initialize player settings and player instance
     * @private
     */

  }, {
    key: 'init',
    value: function init() {
      var _this = this;

      this._debug('Action init');
      this._initOptions = Object.assign({
        autostart: false,
        muted: false,
        fullscreen: false
      }, this._initOptions);
      this._core.setStatus({ seekingTo: null });

      var mediaElement = this.getMediaElement();
      mediaElement.addEventListener('suspend', this._core.getHandler('onSuspend', this));
      mediaElement.addEventListener('loadstart', this._core.getHandler('onLoadstart', this));
      mediaElement.addEventListener('loadeddata', this._core.getHandler('onLoadeddata', this));
      mediaElement.addEventListener('emptied', this._core.getHandler('onEmptied', this));
      mediaElement.addEventListener('abort', this._core.getHandler('onAbort', this));
      mediaElement.addEventListener('canplay', this._core.getHandler('onCanplay', this));
      mediaElement.addEventListener('canplaythrough', this._core.getHandler('onCanplaythrough', this));
      mediaElement.addEventListener('progress', this._core.getHandler('onProgress', this));
      mediaElement.addEventListener('ratechange', this._core.getHandler('onRatechange', this));
      mediaElement.addEventListener('seeking', this._core.getHandler('onSeeking', this));
      mediaElement.addEventListener('seeked', this._core.getHandler('onSeeked', this));
      mediaElement.addEventListener('durationchange', this._core.getHandler('onDurationchange', this));
      mediaElement.addEventListener('waiting', this._core.getHandler('onWaiting', this));
      mediaElement.addEventListener('stalled', this._core.getHandler('onStalled', this));
      mediaElement.addEventListener('play', this._core.getHandler('onPlay', this));
      mediaElement.addEventListener('playing', this._core.getHandler('onPlaying', this));
      mediaElement.addEventListener('pause', this._core.getHandler('onPause', this));
      mediaElement.addEventListener('ended', this._core.getHandler('onEnded', this));
      mediaElement.addEventListener('error', this._core.getHandler('onError', this));
      mediaElement.addEventListener('timeupdate', this._core.getHandler('onTimeupdate', this));
      mediaElement.addEventListener('loadedmetadata', this._core.getHandler('onLoadedmetadata', this));
      mediaElement.addEventListener('volumechange', this._core.getHandler('onVolumechange', this));

      mediaElement.muted = this._initOptions.muted;

      return new Promise(function (resolve) {
        return setTimeout(function () {
          return resolve();
        }, 0);
      }).then(function () {
        return _this._ready();
      });
    }
  }, {
    key: '_removeEventListeners',
    value: function _removeEventListeners() {
      var mediaElement = this.getMediaElement();
      mediaElement.removeEventListener('suspend', this._core.getHandler('suspend'));
      mediaElement.removeEventListener('loadstart', this._core.getHandler('loadstart'));
      mediaElement.removeEventListener('loadeddata', this._core.getHandler('loadeddata'));
      mediaElement.removeEventListener('emptied', this._core.getHandler('emptied'));
      mediaElement.removeEventListener('abort', this._core.getHandler('abort'));
      mediaElement.removeEventListener('canplay', this._core.getHandler('canplay'));
      mediaElement.removeEventListener('canplaythrough', this._core.getHandler('canplaythrough'));
      mediaElement.removeEventListener('progress', this._core.getHandler('progress'));
      mediaElement.removeEventListener('ratechange', this._core.getHandler('ratechange'));
      mediaElement.removeEventListener('seeking', this._core.getHandler('seeking'));
      mediaElement.removeEventListener('seeked', this._core.getHandler('onSeeked'));
      mediaElement.removeEventListener('durationchange', this._core.getHandler('durationchange'));
      mediaElement.removeEventListener('waiting', this._core.getHandler('waiting'));
      mediaElement.removeEventListener('stalled', this._core.getHandler('stalled'));
      mediaElement.removeEventListener('play', this._core.getHandler('play'));
      mediaElement.removeEventListener('playing', this._core.getHandler('playing'));
      mediaElement.removeEventListener('pause', this._core.getHandler('onPause'));
      mediaElement.removeEventListener('ended', this._core.getHandler('ended'));
      mediaElement.removeEventListener('error', this._core.getHandler('error'));
      mediaElement.removeEventListener('timeupdate', this._core.getHandler('timeupdate'));
      mediaElement.removeEventListener('loadedmetadata', this._core.getHandler('loadedmetadata'));
      mediaElement.removeEventListener('volumechange', this._core.getHandler('volumechange'));
    }
  }, {
    key: 'onError',
    value: function onError() /* event */{
      this._emit(this._core.ERROR_EVENT, {
        playerId: this.id,
        pluginName: this.name,
        code: this._mediaElement.error.code,
        message: this._mediaElement.error.message
      });
    }

    /**
     * call this when the player emits a 'ready' event
     * @private
     */

  }, {
    key: '_ready',
    value: function _ready() /* event */{
      var _this2 = this;

      var result = void 0;
      this.isReady = true;
      var status = this._core.getStatus();
      if (status.state === this._core.STATE_UNREADY) {
        this._core.setStatus({ state: this._core.STATE_READY });
      }

      if (this._currentMedia) {
        result = this.beforeStart();
      } else {
        result = Promise.resolve();
      }

      return result.then(function () {
        _this2._emit(_this2._core.STATE_READY);
      });
    }
  }, {
    key: 'getPlayerCurrentTime',
    value: function getPlayerCurrentTime() {
      if (!this._mediaElement) {
        return 0;
      }
      return this._mediaElement.currentTime;
    }
  }, {
    key: 'getCurrentTime',
    value: function getCurrentTime() {
      if (!this._mediaElement) {
        return 0;
      }
      return this._mediaElement.currentTime;
    }
  }, {
    key: 'load',
    value: function load(url) {
      var _this3 = this;

      var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
          _ref$forceStart = _ref.forceStart,
          forceStart = _ref$forceStart === undefined ? false : _ref$forceStart,
          _ref$initTime = _ref.initTime,
          initTime = _ref$initTime === undefined ? 0 : _ref$initTime,
          _ref$autostart = _ref.autostart,
          autostart = _ref$autostart === undefined ? false : _ref$autostart;

      this._debug('Action load(' + url + ')');

      var _core$getCurrentSourc = this._core.getCurrentSource(),
          broadcastType = _core$getCurrentSourc.broadcastType;

      var mediaElement = this.getMediaElement();
      var waitForPlay = mediaElement.paused && autostart;

      if (autostart) {
        mediaElement.setAttribute('autoplay', 'true');
      } else {
        mediaElement.removeAttribute('autoplay');
      }
      var result = Promise.resolve();

      var seekPosition = void 0;

      if (![this._core.BROADCAST_TYPE_LIVE, this._core.BROADCAST_TYPE_TIMESHIFTING].includes(broadcastType)) {
        seekPosition = initTime;
      }

      if (autostart || forceStart) {
        result = result.then(function () {
          return _this3.play(seekPosition);
        });
      } else {
        result = result.then(function () {
          if (!_this3._core.isStopped()) {
            var oncePaused = _this3._core.waitOnce(_this3._core.STATE_PAUSED);
            _this3._mediaElement.currentTime = initTime || 0;
            _this3._mediaElement.pause();
            return oncePaused;
          }
          return Promise.resolve();
        });
      }

      result = result.then(function () {
        mediaElement.src = url;
      });
      if (waitForPlay) {
        result = result.then(function () {
          return new Promise(function (resolve) {
            var listener = function listener() {
              resolve();
              mediaElement.removeEventListener('playing', listener);
            };
            mediaElement.addEventListener('playing', listener);
          });
        });
      }

      return this._debugPromise('start', result);
    }
  }, {
    key: 'play',
    value: function play(time) {
      this._debug('Action play');
      var currentMedia = this._core.getMedia();
      if ((time || typeof time === 'number') && ![this._core.BROADCAST_TYPE_LIVE, this._core.BROADCAST_TYPE_TIMESHIFTING].includes(currentMedia.type)) {
        this._core.setStatus({ seekingTo: time });
        this._mediaElement.currentTime = time;
      }
      var result = this._mediaElement.play();
      return this._debugPromise('start', result);
    }

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'getAvailablePlaybackRates',
    value: function getAvailablePlaybackRates() {
      return [0.25, 0.5, 0.75, 1, 1.25, 1.5, 2];
    }

    /* eslint-enable class-methods-use-this */

  }, {
    key: 'setPlaybackRate',
    value: function setPlaybackRate(speed) {
      this._mediaElement.playbackRate = speed;
      return this._debugPromise('setPlaybackRate', Promise.resolve());
    }
  }, {
    key: 'getPlaybackRate',
    value: function getPlaybackRate() {
      return this._mediaElement.playbackRate;
    }
  }, {
    key: 'pause',
    value: function pause() {
      var result = Promise.resolve();
      this._debug('Action pause');
      if (!this._core.isSourcePauseable(this._core.getCurrentSource())) {
        this._emit(this._core.ACTION_REQUEST_PAUSE);
      } else {
        result = this._core.waitOnce(this._core.STATE_PAUSED);
        this._mediaElement.pause();
      }
      return this._debugPromise('pause', result);
    }
  }, {
    key: 'stop',
    value: function stop() {
      var _this4 = this;

      this._debug('Action stop');
      var result = new Promise(function (resolve) {
        if (_this4._core.isStopped()) {
          var state = _this4._core.getStatus.state;

          if (state !== _this4._core.STATE_STOPPED) {
            _this4._emit(_this4._core.STATE_STOPPED);
            _this4._core.setStatus({
              state: _this4._core.STATE_STOPPED
            });
          }
          resolve();
        } else {
          _this4._core.once(_this4._core.STATE_STOPPED, function () {
            _this4._debug('Action on est ici');

            return resolve();
          });
          _this4._core.setStatus({
            isStopping: true
          });
          _this4._debug('instance pause (for stop action)');
          _this4._mediaElement.pause();
        }
      });

      return this._debugPromise('stop', result);
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      var _this5 = this;

      var result = void 0;
      this._debug('Action destroy');
      var media = this.getMediaElement();
      if (!media) {
        result = Promise.resolve();
      } else {
        result = this.stop().then(function () {
          _this5._removeEventListeners();
          var parent = _this5.getParent();
          parent.removeChild(media);
          _this5._mediaElement = null;
        });
      }
      return this._debugPromise('destroy', result);
    }

    /**
     * @param {integer} volume value between 0 and 1
     */

  }, {
    key: 'setVolume',
    value: function setVolume(volume) {
      this._debug('Action setVolume');
      this._mediaElement.volume = volume;
      this._emit(this._core.VOLUME_UPDATED);
    }
  }, {
    key: 'getVolume',
    value: function getVolume() {
      return this._mediaElement.volume;
    }
  }, {
    key: 'setMute',
    value: function setMute() {
      this._debug('Action setMute');
      this._mediaElement.muted = !this._mediaElement.muted;
    }
  }, {
    key: 'isMuted',
    value: function isMuted() {
      return !!this._mediaElement.muted;
    }
  }, {
    key: 'name',
    get: function get() {
      return Html5SoundAdapter.name;
    } // eslint-disable-line class-methods-use-this

  }], [{
    key: 'getPluginMetadata',
    value: function getPluginMetadata() {
      return {
        version: _package2.default.version,
        minimumCoreVersion: _package2.default['rfplayer-plugin'].minimumCoreVersion
      };
    }
  }, {
    key: 'getListMediaType',
    value: function getListMediaType() {
      return MEDIA_TYPES;
    }
  }, {
    key: 'canPlay',
    value: function canPlay(source, Transistor) {
      return Transistor.canPlay(source, this.getListMediaType());
    }
  }, {
    key: 'init',
    value: function init(core) {
      core.addAdapter(Html5SoundAdapter);
    }
  }, {
    key: 'name',
    get: function get() {
      return 'Html5SoundAdapter';
    }
  }]);

  return Html5SoundAdapter;
}();

exports.default = Html5SoundAdapter;

/***/ })

/******/ });
});
//# sourceMappingURL=index.js.map